package at.faktorzehn.pmo;

import org.linkki.core.pmo.ModelObject;
import org.linkki.core.ui.element.annotation.UIComboBox;
import org.linkki.core.ui.element.annotation.UIIntegerField;
import org.linkki.core.ui.layout.annotation.UISection;

import at.faktorzehn.kfz.model.Fahrzeug;

@UISection(caption = "Fahrzeug Eingabe")
public class FahrzeugPmo {
	private Fahrzeug fahrzeug;

	public FahrzeugPmo(Fahrzeug fahrzeug) {
		this.fahrzeug = fahrzeug;
	}

	@ModelObject
	public Fahrzeug getFahrzeug() {
		return fahrzeug;
	}

	@UIIntegerField(position = 10, modelAttribute = Fahrzeug.PROPERTY_PS)
	public void getPs() {
	}

	@UIComboBox(position = 20, modelAttribute = Fahrzeug.PROPERTY_MARKE)
	public void marke() {

	}
}
