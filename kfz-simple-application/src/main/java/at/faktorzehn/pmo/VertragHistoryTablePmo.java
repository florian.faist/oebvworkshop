package at.faktorzehn.pmo;

import java.util.List;
import java.util.function.Supplier;

import org.linkki.core.defaults.columnbased.pmo.ContainerPmo;
import org.linkki.core.defaults.columnbased.pmo.SimpleItemSupplier;
import org.linkki.core.ui.layout.annotation.UISection;

import at.faktorzehn.kfz.model.KfzVertrag;

@UISection(caption = "Berechnungs Historie")
public class VertragHistoryTablePmo implements ContainerPmo<VertragRowPmo> {

	private final SimpleItemSupplier<VertragRowPmo, KfzVertrag> items;

	public VertragHistoryTablePmo(Supplier<List<KfzVertrag>> vertraege) {
		items = new SimpleItemSupplier<VertragRowPmo, KfzVertrag>(vertraege, VertragRowPmo::new);
	}

	@Override
	public List<VertragRowPmo> getItems() {
		return items.get();
	}

	@Override
	public int getPageLength() {
		return Math.min(ContainerPmo.super.getPageLength(), getItems().size());
	}

}
