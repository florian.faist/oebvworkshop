package at.faktorzehn.pmo;

import org.linkki.core.defaults.ui.aspects.types.EnabledType;
import org.linkki.core.pmo.ModelObject;
import org.linkki.core.ui.element.annotation.UIDoubleField;
import org.linkki.core.ui.layout.annotation.UISection;

import at.faktorzehn.kfz.model.KfzVertrag;

@UISection(caption = "Vertrags Daten")
public class VertragPmo {
	private KfzVertrag kfzVertrag;

	public VertragPmo(KfzVertrag kfzVertrag) {
		this.kfzVertrag = kfzVertrag;
	}

	@ModelObject
	public KfzVertrag getKfzVertrag() {
		return kfzVertrag;
	}

	@UIDoubleField(position = 10, modelAttribute = KfzVertrag.PROPERTY_PREIS, enabled = EnabledType.DYNAMIC)
	public void preis() {
	}

	public boolean isPreisEnabled() {
		return false;
	}
}
