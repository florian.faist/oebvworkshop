package at.faktorzehn.pmo;

import java.util.Optional;

import org.linkki.core.pmo.ModelObject;
import org.linkki.core.ui.element.annotation.UIDoubleField;
import org.linkki.core.ui.element.annotation.UILabel;
import org.linkki.core.ui.element.annotation.UITextField;

import at.faktorzehn.kfz.model.Fahrzeug;
import at.faktorzehn.kfz.model.FahrzeugMarken;
import at.faktorzehn.kfz.model.KfzVertrag;

public class VertragRowPmo {
	private final KfzVertrag vertrag;

	public VertragRowPmo(KfzVertrag vertrag) {
		this.vertrag = vertrag;
	}

	@ModelObject
	public KfzVertrag getCar() {
		return vertrag;
	}

	@ModelObject(name = "fahrzeug")
	public Fahrzeug getFahrzeug() {
		return vertrag.getFahrzeug();
	}

	@UILabel(position = 10, label = "Erstellungsdatum", modelAttribute = KfzVertrag.PROPERTY_CREATEDATE)
	public void createDate() {
		/* model binding */
	}

	@UIDoubleField(position = 20, label = "Preis", modelAttribute = KfzVertrag.PROPERTY_PREIS)
	public void preis() {
		/* model binding */
	}

	@UIDoubleField(position = 30, label = "PS", modelObject = "fahrzeug", modelAttribute = Fahrzeug.PROPERTY_PS)
	public void ps() {
		/* model binding */
	}

	@UITextField(position = 40, label = "Marke")
	public String getMarke() {
		return Optional.ofNullable(getFahrzeug().getMarke()).map(FahrzeugMarken::name).orElse("");
	}
}
