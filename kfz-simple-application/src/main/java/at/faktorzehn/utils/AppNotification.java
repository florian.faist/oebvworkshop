package at.faktorzehn.utils;

import org.linkki.core.binding.validation.message.Message;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;

public class AppNotification {

	public static void show(Message message) {
		Notification notify = new Notification();
		switch (message.getSeverity()) {
		case ERROR:
			notify.addThemeVariants(NotificationVariant.LUMO_ERROR);
			break;
		case WARNING:
			break;
		default:
			notify.addThemeVariants(NotificationVariant.LUMO_PRIMARY);
		}
		notify.setText(message.getText());
		notify.setDuration(2500);
		notify.open();
	}

}
