package at.faktorzehn.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import at.faktorzehn.service.FipsService;

@Configuration
public class BeanRegistry {

	@Bean
	public FipsService getFipsService() {
		return new FipsService();
	}

}
