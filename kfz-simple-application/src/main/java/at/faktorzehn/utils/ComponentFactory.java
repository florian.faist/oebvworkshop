package at.faktorzehn.utils;

import java.util.List;
import java.util.function.Supplier;

import org.linkki.core.binding.BindingContext;
import org.linkki.core.ui.creation.VaadinUiCreator;

import com.vaadin.flow.component.Component;

import at.faktorzehn.kfz.model.Fahrzeug;
import at.faktorzehn.kfz.model.KfzVertrag;
import at.faktorzehn.pmo.FahrzeugPmo;
import at.faktorzehn.pmo.VertragHistoryTablePmo;
import at.faktorzehn.pmo.VertragPmo;

public class ComponentFactory {

	public static Component getFahrzeugPmo(Fahrzeug fahrzeug, BindingContext fahrzeugBindingContext) {
		return VaadinUiCreator.createComponent(new FahrzeugPmo(fahrzeug), fahrzeugBindingContext);
	}

	public static Component getVertragPmo(KfzVertrag vertrag, BindingContext ctx) {
		return VaadinUiCreator.createComponent(new VertragPmo(vertrag), ctx);
	}

	public static Component getVertragHistoryTablePmo(Supplier<List<KfzVertrag>> vertraege, BindingContext ctx) {
		return VaadinUiCreator.createComponent(new VertragHistoryTablePmo(vertraege), ctx);
	}
}
