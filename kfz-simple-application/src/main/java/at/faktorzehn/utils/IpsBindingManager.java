package at.faktorzehn.utils;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.faktorips.runtime.IModelObject;
import org.linkki.core.binding.dispatcher.behavior.PropertyBehaviorProvider;
import org.linkki.core.binding.manager.DefaultBindingManager;
import org.linkki.ips.binding.dispatcher.IpsPropertyDispatcherFactory;

public class IpsBindingManager extends DefaultBindingManager {
	private Consumer<IModelObject> afterUpdateObserver;
	private Supplier<IModelObject> modelSupplier;

	public IpsBindingManager(Supplier<IModelObject> modelSupplier, Consumer<IModelObject> afterUpdateObserver) {
		super(new IpsBindingValidationService(modelSupplier), PropertyBehaviorProvider.NO_BEHAVIOR_PROVIDER,
				new IpsPropertyDispatcherFactory());
		this.afterUpdateObserver = afterUpdateObserver;
		this.modelSupplier = modelSupplier;
	}

	@Override
	public void afterUpdateUi() {
		super.afterUpdateUi();
		this.afterUpdateObserver.accept(modelSupplier.get());
	}

}
