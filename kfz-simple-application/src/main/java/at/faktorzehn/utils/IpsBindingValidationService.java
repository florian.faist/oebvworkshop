package at.faktorzehn.utils;

import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.faktorips.runtime.IModelObject;
import org.faktorips.runtime.ValidationContext;
import org.linkki.core.binding.validation.ValidationService;
import org.linkki.core.binding.validation.message.MessageList;
import org.linkki.ips.messages.MessageConverter;

import com.vaadin.flow.component.UI;

public class IpsBindingValidationService implements ValidationService {

	private final Supplier<IModelObject> vertragSupplier;

	public IpsBindingValidationService(Supplier<IModelObject> vertragSupplier) {
		this.vertragSupplier = vertragSupplier;
	}

	@Override
	public MessageList getValidationMessages() {
		return notifyNotFieldBoundedMessages(MessageConverter
				.convert(vertragSupplier.get().validate(new ValidationContext(UI.getCurrent().getLocale()))));
	}

	private MessageList notifyNotFieldBoundedMessages(MessageList list) {
		filterAndDisplayAppNotificationMessages(list);
		return list;
	}

	private void filterAndDisplayAppNotificationMessages(MessageList list) {
		list.stream().filter(msg -> msg.getInvalidObjectProperties().stream() //
				.filter(obj -> StringUtils.isEmpty(obj.getProperty())) //
				.findFirst() //
				.isPresent()) //
				.forEach(AppNotification::show);
	}
}
