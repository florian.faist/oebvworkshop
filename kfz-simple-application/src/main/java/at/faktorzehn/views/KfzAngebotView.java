package at.faktorzehn.views;

import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.linkki.core.binding.BindingContext;
import org.linkki.core.binding.manager.BindingManager;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import at.faktorzehn.kfz.model.Fahrzeug;
import at.faktorzehn.kfz.model.KfzVertrag;
import at.faktorzehn.service.FipsService;
import at.faktorzehn.utils.ComponentFactory;
import at.faktorzehn.utils.IpsBindingManager;

@PageTitle("KfzAngebot")
@Route(value = "kfzangebot", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
public class KfzAngebotView extends VerticalLayout implements BeforeEnterObserver {
	private final FipsService fipsService;
	private KfzVertrag vertrag;

	public KfzAngebotView(FipsService fipsService) {
		this.fipsService = fipsService;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		var vtgListId = event.getLocation().getQueryParameters().getParameters()
				.getOrDefault("vertragid", Collections.singletonList("")).get(0);
		loadContractIfExistsOrCreate(vtgListId);
		initView();
	}

	private void initView() {
		var bindingManager = createBindingManager();
		var ctx = createBindingCtx(bindingManager);
		Fahrzeug fahrzeug = createFahrzeugAndSetToVertrag(vertrag);

		add(new HorizontalLayout(ComponentFactory.getVertragPmo(vertrag, ctx),
				ComponentFactory.getFahrzeugPmo(fahrzeug, ctx)));
		add(ComponentFactory
				.getVertragHistoryTablePmo(() -> fipsService.getKfzVertragCalculationHistory(vertrag.getId()), ctx));
	}

	private Fahrzeug createFahrzeugAndSetToVertrag(KfzVertrag vertrag) {
		Fahrzeug fahrzeug;
		if (vertrag.getFahrzeug() == null) {
			fahrzeug = vertrag.getKfzProdukt().getFahrzeugTyp().createFahrzeug();
			vertrag.setFahrzeug(fahrzeug);
		} else {
			fahrzeug = vertrag.getFahrzeug();
		}
		return fahrzeug;
	}

	private void loadContractIfExistsOrCreate(String contractId) {
		if (StringUtils.isNotEmpty(contractId)) {
			vertrag = fipsService.getContract(contractId);
		}
		createContractIfNull();
	}

	private void createContractIfNull() {
		if (vertrag == null) {
			vertrag = fipsService.creatVertrag();
		}
	}

	private BindingManager createBindingManager() {
		return new IpsBindingManager(() -> vertrag, data -> fipsService.saveContract((KfzVertrag) data));
	}

	private BindingContext createBindingCtx(BindingManager manager) {
		return manager.getContext(KfzVertrag.class);
	}
}
