package at.faktorzehn.repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import at.faktorzehn.kfz.model.KfzVertrag;

public class KfzVertragInMemRepo {
	public Map<String, List<KfzVertrag>> contractHistoryRepo = new HashMap<>();

	public void save(KfzVertrag vertrag) {
		generateUUIDIfNotExists(vertrag);
		vertrag.setCreateDate(LocalDateTime.now());
		var historyList = contractHistoryRepo.getOrDefault(vertrag.getId(), new ArrayList<KfzVertrag>());
		historyList.add(vertrag.newCopy());
		contractHistoryRepo.put(vertrag.getId(), historyList);
	}

	public List<KfzVertrag> getContractHistory(String id) {
		return Collections.unmodifiableList(contractHistoryRepo.getOrDefault(id, new ArrayList<KfzVertrag>()));
	}

	public KfzVertrag getLatestById(String id) {
		return getContractHistory(id).stream().max((v1, v2) -> v1.getCreateDate().compareTo(v2.getCreateDate()))
				.orElse(null);
	}

	private void generateUUIDIfNotExists(KfzVertrag vertrag) {
		if (StringUtils.isEmpty(vertrag.getId())) {
			vertrag.setId(UUID.randomUUID().toString());
		}
	}
}
