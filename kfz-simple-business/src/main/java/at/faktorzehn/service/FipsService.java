package at.faktorzehn.service;

import java.util.List;

import org.faktorips.runtime.Severity;
import org.faktorips.runtime.ValidationContext;

import at.faktorzehn.export.KfzProductRepository;
import at.faktorzehn.export.VertragFactory;
import at.faktorzehn.kfz.model.KfzVertrag;
import at.faktorzehn.repository.KfzVertragInMemRepo;

public class FipsService {
	private KfzProductRepository productFactory = new KfzProductRepository(this.getClass().getClassLoader());
	private KfzVertragInMemRepo kfzVertragInMemRepo = new KfzVertragInMemRepo();

	public KfzVertrag creatVertrag() {
		var vertragFactory = new VertragFactory(productFactory.getKfzProduct());
		return vertragFactory.createKfzVertrag();
	}

	public void saveContract(KfzVertrag vertrag) {
		if (vertrag.validate(new ValidationContext()).getMessagesBySeverity(Severity.ERROR).isEmpty()) {
			kfzVertragInMemRepo.save(vertrag);
			System.out.println("KFZ Mit UUID Saved" + vertrag.getId());
		}
	}

	public List<KfzVertrag> getKfzVertragCalculationHistory(String kfzVertragId) {
		System.out.println("KFZ read UUID" + kfzVertragId);
		return kfzVertragInMemRepo.getContractHistory(kfzVertragId);
	}

	public KfzVertrag getContract(String contractId) {
		return kfzVertragInMemRepo.getLatestById(contractId);
	}
}
