import '@vaadin/button';
import '@vaadin/notification';
import { Notification } from '@vaadin/notification';
import '@vaadin/text-field';
import { html } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { View } from '../../views/view';
import '@vaadin/combo-box';
import '@vaadin/number-field';
import { Binder, field } from '@vaadin/form';
import KfzVertragDto from 'Frontend/generated/at/faktorzehn/dto/KfzVertragDto';
import KfzVertragDtoModel from 'Frontend/generated/at/faktorzehn/dto/KfzVertragDtoModel';
import KfzProduktDto from 'Frontend/generated/at/faktorzehn/dto/KfzProduktDto';
import { createVertrag, updateVertrag } from 'Frontend/generated/FipsServiceEndpoint';
@customElement('hello-world-view')
export class HelloWorldView extends View {
  @state()
  private vertrag: KfzVertragDto | undefined = undefined;

  private binder = new Binder(this, KfzVertragDtoModel, {
    onSubmit: updateVertrag
  });

  connectedCallback() {
    super.connectedCallback();
    this.classList.add('flex', 'p-m', 'gap-m', 'items-end');
  }

  render() {
    return html`
    <vaadin-number-field label="PS" ...='${field(this.binder.model.fahrzeugDto.ps)}'>
      <div slot="suffix">PS</div>
    </vaadin-number-field>
    <vaadin-combo-box label="Marke" ...='${field(this.binder.model?.fahrzeugDto?.marke)}' .items="${this.vertrag?.kfzProdukt?.fahrzeugmarken}" ></vaadin-combo-box>
    <vaadin-number-field readonly="true" label="Preis" ...='${field(this.binder.model.preis)}'>
      <div slot="suffix">€</div>
    </vaadin-number-field>
    <vaadin-button theme="primary" @click="${this.updateVertrag}">Rechnen</vaadin-button>
    `;
  }

  async firstUpdated() {
    const vtg = await createVertrag();
    this.vertrag = vtg;
    this.binder.read(this.vertrag as KfzVertragDto);
  }

  async updateVertrag() {
    const result = await this.binder.submit();
    this.vertrag = (result as KfzVertragDto);
    this.binder.read(this.vertrag);
  }
}