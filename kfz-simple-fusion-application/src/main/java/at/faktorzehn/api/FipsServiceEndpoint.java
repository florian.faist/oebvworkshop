package at.faktorzehn.api;

import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.vaadin.fusion.Endpoint;

import at.faktorzehn.dto.KfzVertragDto;
import at.faktorzehn.mapper.VertragsMapper;
import at.faktorzehn.service.FipsService;

@Endpoint
@AnonymousAllowed
public class FipsServiceEndpoint {
	private final FipsService fipsService;
	private VertragsMapper mapper = new VertragsMapper();

	public FipsServiceEndpoint(FipsService fipsService) {
		this.fipsService = fipsService;
	}

	public KfzVertragDto createVertrag() {
		var vertrag = fipsService.creatVertrag();
		fipsService.saveContract(vertrag);
		return mapper.map(vertrag);
	}

	public KfzVertragDto updateVertrag(KfzVertragDto kfzVertrag) {
		var vertragDB = fipsService.getContract(kfzVertrag.getId());
		if (vertragDB == null) {
			vertragDB = fipsService.creatVertrag();
		}
		mapper.update(vertragDB, kfzVertrag);

		fipsService.saveContract(vertragDB);
		return mapper.map(vertragDB);
	}
}
