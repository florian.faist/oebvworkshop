package at.faktorzehn.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FahrzeugDto {
	private Integer ps;
	private String marke;

}
