package at.faktorzehn.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KfzProduktDto {

	private List<String> fahrzeugmarken;
	private List<Integer> erlaubteZahlweise;
}
