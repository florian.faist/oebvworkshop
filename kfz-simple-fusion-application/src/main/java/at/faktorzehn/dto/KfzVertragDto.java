package at.faktorzehn.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KfzVertragDto {
	private String id;
	private BigDecimal zahlweise;
	private BigDecimal preis;
	private LocalDateTime cretaeDate;
	private KfzProduktDto kfzProdukt = new KfzProduktDto();
	private FahrzeugDto fahrzeugDto = new FahrzeugDto();
}
