package at.faktorzehn.mapper;

import org.faktorips.runtime.IModelObject;

public interface TwoWayModelBinding<SRC extends IModelObject, DEST> {

	DEST map(SRC src);

	void update(SRC src, DEST dest);
}
