package at.faktorzehn.mapper;

import java.util.Optional;
import java.util.stream.Collectors;

import at.faktorzehn.dto.FahrzeugDto;
import at.faktorzehn.dto.KfzProduktDto;
import at.faktorzehn.dto.KfzVertragDto;
import at.faktorzehn.kfz.model.Fahrzeug;
import at.faktorzehn.kfz.model.FahrzeugMarken;
import at.faktorzehn.kfz.model.KfzProdukt;
import at.faktorzehn.kfz.model.KfzVertrag;

public class VertragsMapper implements TwoWayModelBinding<KfzVertrag, KfzVertragDto> {

	@Override
	public KfzVertragDto map(KfzVertrag src) {
		KfzVertragDto dto = new KfzVertragDto();
		dto.setCretaeDate(src.getCreateDate());
		dto.setId(src.getId());
		dto.setKfzProdukt(mapKfzProdukt(src.getKfzProdukt()));
		dto.setPreis(src.getPreis());
		dto.setFahrzeugDto(createFahrzeugDto(src));
		return dto;
	}

	@Override
	public void update(KfzVertrag src, KfzVertragDto dest) {
		mapFahrzeug(src, dest.getFahrzeugDto());
	}

	private void mapFahrzeug(KfzVertrag vertrag, FahrzeugDto fahrzeugDto) {
		Fahrzeug fahrzeug = getOrCreateFahrzeug(vertrag);
		fahrzeug.setMarke(FahrzeugMarken.valueOf(fahrzeugDto.getMarke()));
		fahrzeug.setPS(fahrzeugDto.getPs());
		vertrag.setFahrzeug(fahrzeug);
	}

	private KfzProduktDto mapKfzProdukt(KfzProdukt kfzProdukt) {
		KfzProduktDto produktDto = new KfzProduktDto();
		produktDto.setFahrzeugmarken(kfzProdukt.getFahrzeugTyp().getMarken().stream().map(FahrzeugMarken::getName)
				.collect(Collectors.toList()));
		return produktDto;
	}

	private FahrzeugDto createFahrzeugDto(KfzVertrag vertrag) {
		Fahrzeug fahrzeug = getOrCreateFahrzeug(vertrag);
		FahrzeugDto dto = new FahrzeugDto();
		dto.setMarke(Optional.ofNullable(fahrzeug.getMarke()).map(FahrzeugMarken::getName).orElse(null));
		dto.setPs(fahrzeug.getPS());
		return dto;
	}

	private Fahrzeug getOrCreateFahrzeug(KfzVertrag vertrag) {
		return Optional.ofNullable(vertrag.getFahrzeug()).stream().map(Fahrzeug.class::cast).findAny()
				.orElse(vertrag.getKfzProdukt().getFahrzeugTyp().createFahrzeug());
	}
}
