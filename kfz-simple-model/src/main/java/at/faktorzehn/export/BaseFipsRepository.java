package at.faktorzehn.export;

import org.faktorips.runtime.ClassloaderRuntimeRepository;
import org.faktorips.runtime.formula.groovy.GroovyFormulaEvaluatorFactory;

public abstract class BaseFipsRepository {
	private final ClassLoader clazzloader;

	public BaseFipsRepository() {
		this.clazzloader = null;

	}

	public BaseFipsRepository(ClassLoader clazzloader) {
		this.clazzloader = clazzloader;
	}

	protected ClassloaderRuntimeRepository createRepository(String tocPath) {
		ClassloaderRuntimeRepository repository;
		if (clazzloader != null) {
			repository = ClassloaderRuntimeRepository.create(tocPath, clazzloader);
		} else {
			repository = ClassloaderRuntimeRepository.create(tocPath);
		}

		repository.setFormulaEvaluatorFactory(new GroovyFormulaEvaluatorFactory());
		return repository;
	}
}
