package at.faktorzehn.export;

import org.faktorips.runtime.IRuntimeRepository;
import org.faktorips.runtime.internal.AbstractRuntimeRepository;

public class KfzModelRepository extends BaseFipsRepository {

	private IRuntimeRepository repository;

	public KfzModelRepository() {
		super();
	}

	public KfzModelRepository(ClassLoader clazzloader) {
		super(clazzloader);

	}

	public IRuntimeRepository getModelRepository() {
		if (repository == null) {
			repository = createKfzModelRepository();
		}
		return repository;
	}

	/**
	 * Erzeugt {@link AbstractRuntimeRepository}, das alle Repositories der
	 * KFZ-Versicherungsmodells in einem Methodenaufruf integriert.
	 * <p>
	 * Aufgrund der IOS-Persistenz werden neben den IPS-Produktdatenprojekten
	 * zusätzlich IPS-Modellprojekte hinzugezogen.
	 */
	private AbstractRuntimeRepository createKfzModelRepository() {
		return createRepository("at/faktorzehn/kfz/model/internal/faktorips-repository-toc.xml");

	}
}
