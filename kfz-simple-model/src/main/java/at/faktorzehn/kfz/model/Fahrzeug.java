package at.faktorzehn.kfz.model;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.faktorips.runtime.DefaultUnresolvedReference;
import org.faktorips.runtime.IConfigurableModelObject;
import org.faktorips.runtime.ICopySupport;
import org.faktorips.runtime.IDeltaComputationOptions;
import org.faktorips.runtime.IDeltaSupport;
import org.faktorips.runtime.IDependantObject;
import org.faktorips.runtime.IModelObject;
import org.faktorips.runtime.IModelObjectDelta;
import org.faktorips.runtime.IModelObjectVisitor;
import org.faktorips.runtime.IObjectReferenceStore;
import org.faktorips.runtime.IProductComponent;
import org.faktorips.runtime.IRuntimeRepository;
import org.faktorips.runtime.IUnresolvedReference;
import org.faktorips.runtime.IValidationContext;
import org.faktorips.runtime.IVisitorSupport;
import org.faktorips.runtime.Message;
import org.faktorips.runtime.MessageList;
import org.faktorips.runtime.ObjectProperty;
import java.util.List;
import java.util.Arrays;
import org.faktorips.runtime.Severity;
import org.faktorips.runtime.annotation.IpsGenerated;
import org.faktorips.runtime.internal.AbstractModelObject;
import org.faktorips.runtime.internal.IpsStringUtils;
import org.faktorips.runtime.internal.ModelObjectDelta;
import org.faktorips.runtime.internal.ProductConfiguration;
import org.faktorips.runtime.internal.XmlCallback;
import org.faktorips.runtime.model.annotation.IpsAssociation;
import org.faktorips.runtime.model.annotation.IpsAssociationAdder;
import org.faktorips.runtime.model.annotation.IpsAssociations;
import org.faktorips.runtime.model.annotation.IpsAttribute;
import org.faktorips.runtime.model.annotation.IpsAttributeSetter;
import org.faktorips.runtime.model.annotation.IpsAttributes;
import org.faktorips.runtime.model.annotation.IpsConfiguredBy;
import org.faktorips.runtime.model.annotation.IpsConfiguredValidationRule;
import org.faktorips.runtime.model.annotation.IpsDocumented;
import org.faktorips.runtime.model.annotation.IpsInverseAssociation;
import org.faktorips.runtime.model.annotation.IpsPolicyCmptType;
import org.faktorips.runtime.model.annotation.IpsValidationRule;
import org.faktorips.runtime.model.annotation.IpsValidationRules;
import org.faktorips.runtime.model.type.AssociationKind;
import org.faktorips.runtime.model.type.AttributeKind;
import org.faktorips.runtime.model.type.ValueSetKind;
import org.faktorips.runtime.util.MessagesHelper;
import org.faktorips.valueset.IntegerRange;
import org.w3c.dom.Element;

/**
 * Implementation for Fahrzeug.
 * 
 * @since 1.0.0
 *
 * @generated
 */
@IpsPolicyCmptType(name = "Fahrzeug")
@IpsAttributes({ "marke", "PS" })
@IpsAssociations({ "KfzVertrag", "HaftpflichtVersicherung", "KaskoVersicherung" })
@IpsConfiguredBy(FahrzeugTyp.class)
@IpsValidationRules({ "bmwNur200PsRegel", "vw400PsAnfragePflichtig" })
@IpsDocumented(bundleName = "at.faktorzehn.kfz.model.model-label-and-descriptions", defaultLocale = "de")
public class Fahrzeug extends AbstractModelObject
		implements IDeltaSupport, ICopySupport, IVisitorSupport, IDependantObject, IConfigurableModelObject {

	/**
	 * The name of the association kfzVertrag.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String ASSOCIATION_KFZ_VERTRAG = "kfzVertrag";
	/**
	 * The maximal multiplicity of the association with the role name
	 * HaftpflichtVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final IntegerRange MAX_MULTIPLICITY_OF_HAFTPFLICHT_VERSICHERUNG = IntegerRange.valueOf(0, 1);
	/**
	 * The name of the association haftpflichtVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String ASSOCIATION_HAFTPFLICHT_VERSICHERUNG = "haftpflichtVersicherung";
	/**
	 * The maximal multiplicity of the association with the role name
	 * KaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final IntegerRange MAX_MULTIPLICITY_OF_KASKO_VERSICHERUNG = IntegerRange.valueOf(0, 1);
	/**
	 * The name of the association kaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String ASSOCIATION_KASKO_VERSICHERUNG = "kaskoVersicherung";
	/**
	 * Error code for rule bmwNur200PsRegel.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String MSG_CODE_BMW_NUR200_PS_REGEL = "error.Fahrzeug.bmwNur200PsRegel";
	/**
	 * Error code for rule vw400PsAnfragePflichtig.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String MSG_CODE_VW400_PS_ANFRAGE_PFLICHTIG = "warning.Fahrzeug.vw400PsAnfragePflichtig";
	/**
	 * The name of the rule vw400PsAnfragePflichtig.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String RULE_VW400_PS_ANFRAGE_PFLICHTIG = "vw400PsAnfragePflichtig";
	/**
	 * The name of the property marke.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_MARKE = "marke";
	/**
	 * The name of the property PS.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_PS = "PS";
	/**
	 * Member variable for marke.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private FahrzeugMarken marke = null;

	/**
	 * Member variable for PS.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private Integer pS = null;

	/**
	 * References the current product configuration.
	 *
	 * @generated
	 */
	private ProductConfiguration productConfiguration;

	/**
	 * Member variable for the parent object: KfzVertrag.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private KfzVertrag kfzVertrag;

	/**
	 * Member variable for the association with the role name
	 * HaftpflichtVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private HaftpflichtVersicherung haftpflichtVersicherung = null;

	/**
	 * Member variable for the association with the role name KaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private KaskoVersicherung kaskoVersicherung = null;

	/**
	 * Creates a new Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public Fahrzeug() {
		super();
		productConfiguration = new ProductConfiguration();
	}

	/**
	 * Creates a new Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public Fahrzeug(FahrzeugTyp productCmpt) {
		super();
		productConfiguration = new ProductConfiguration(productCmpt);
	}

	/**
	 * Returns the marke.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttribute(name = "marke", kind = AttributeKind.CHANGEABLE, valueSetKind = ValueSetKind.AllValues)
	@IpsGenerated
	public FahrzeugMarken getMarke() {
		return marke;
	}

	/**
	 * Sets the value of attribute marke.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttributeSetter("marke")
	@IpsGenerated
	public void setMarke(FahrzeugMarken newValue) {
		this.marke = newValue;
	}

	/**
	 * Returns the PS.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttribute(name = "PS", kind = AttributeKind.CHANGEABLE, valueSetKind = ValueSetKind.AllValues)
	@IpsGenerated
	public Integer getPS() {
		return pS;
	}

	/**
	 * Sets the value of attribute PS.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttributeSetter("PS")
	@IpsGenerated
	public void setPS(Integer newValue) {
		this.pS = newValue;
	}

	/**
	 * Returns the referenced KfzVertrag.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "KfzVertrag", pluralName = "KfzVertraege", kind = AssociationKind.CompositionToMaster, targetClass = KfzVertrag.class, min = 0, max = 1)
	@IpsInverseAssociation("Fahrzeug")
	@IpsGenerated
	public KfzVertrag getKfzVertrag() {
		return kfzVertrag;
	}

	/**
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "KfzVertrag")
	@IpsGenerated
	public void setKfzVertragInternal(KfzVertrag newParent) {
		if (getKfzVertrag() == newParent) {
			return;
		}
		IModelObject parent = getParentModelObject();
		if (newParent != null && parent != null) {
			throw new IllegalStateException(String.format(
					"Fahrzeug (\"%s\") can't be assigned to parent object of class KfzVertrag (\"%s\"), because object already belongs to the parent object (\"%s\").",
					toString(), newParent.toString(), parent.toString()));
		}
		this.kfzVertrag = newParent;
		effectiveFromHasChanged();
	}

	/**
	 * Returns the referenced HaftpflichtVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "HaftpflichtVersicherung", pluralName = "HaftpflichtVersicherungen", kind = AssociationKind.Association, targetClass = HaftpflichtVersicherung.class, min = 0, max = 1)
	@IpsInverseAssociation("Fahrzeug")
	@IpsGenerated
	public HaftpflichtVersicherung getHaftpflichtVersicherung() {
		return haftpflichtVersicherung;
	}

	/**
	 * Sets the HaftpflichtVersicherung.
	 * 
	 * @throws ClassCastException If the association is constrained and the given
	 *                            object is not of the correct type.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "HaftpflichtVersicherung")
	@IpsGenerated
	public void setHaftpflichtVersicherung(HaftpflichtVersicherung newObject) {
		if (newObject == haftpflichtVersicherung) {
			return;
		}
		HaftpflichtVersicherung oldRefObject = haftpflichtVersicherung;
		haftpflichtVersicherung = null;
		if (oldRefObject != null) {
			oldRefObject.setFahrzeug(null);
		}
		haftpflichtVersicherung = newObject;
		if (haftpflichtVersicherung != null && haftpflichtVersicherung.getFahrzeug() != this) {
			haftpflichtVersicherung.setFahrzeug(this);
		}
	}

	/**
	 * Returns the referenced KaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "KaskoVersicherung", pluralName = "KaskoVersicherungen", kind = AssociationKind.Association, targetClass = KaskoVersicherung.class, min = 0, max = 1)
	@IpsInverseAssociation("Fahrzeug")
	@IpsGenerated
	public KaskoVersicherung getKaskoVersicherung() {
		return kaskoVersicherung;
	}

	/**
	 * Sets the KaskoVersicherung.
	 * 
	 * @throws ClassCastException If the association is constrained and the given
	 *                            object is not of the correct type.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "KaskoVersicherung")
	@IpsGenerated
	public void setKaskoVersicherung(KaskoVersicherung newObject) {
		if (newObject == kaskoVersicherung) {
			return;
		}
		KaskoVersicherung oldRefObject = kaskoVersicherung;
		kaskoVersicherung = null;
		if (oldRefObject != null) {
			oldRefObject.setFahrzeug(null);
		}
		kaskoVersicherung = newObject;
		if (kaskoVersicherung != null && kaskoVersicherung.getFahrzeug() != this) {
			kaskoVersicherung.setFahrzeug(this);
		}
	}

	/**
	 * Initializes the object with the configured defaults.
	 *
	 * @restrainedmodifiable
	 */
	@Override
	@IpsGenerated
	public void initialize() {
		// begin-user-code
		// end-user-code
	}

	/**
	 * Returns the FahrzeugTyp that configures this object.
	 *
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugTyp getFahrzeugTyp() {
		return (FahrzeugTyp) getProductComponent();
	}

	/**
	 * Sets the new FahrzeugTyp that configures this object.
	 * 
	 * @param fahrzeugTyp                            The new FahrzeugTyp.
	 * @param initPropertiesWithConfiguratedDefaults <code>true</code> if the
	 *                                               properties should be
	 *                                               initialized with the defaults
	 *                                               defined in the FahrzeugTyp.
	 *
	 * @generated
	 */
	@IpsGenerated
	public void setFahrzeugTyp(FahrzeugTyp fahrzeugTyp, boolean initPropertiesWithConfiguratedDefaults) {
		setProductComponent(fahrzeugTyp);
		if (initPropertiesWithConfiguratedDefaults) {
			initialize();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IProductComponent getProductComponent() {
		return productConfiguration.getProductComponent();
	}

	/**
	 * Sets the current product component.
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public void setProductComponent(IProductComponent productComponent) {
		productConfiguration.setProductComponent(productComponent);
	}

	/**
	 * This method is called when the effective from date has changed, so that the
	 * reference to the product component generation can be cleared. If this policy
	 * component contains child components, this method will also clear the
	 * reference to their product component generations.
	 * <p>
	 * The product component generation is cleared if and only if there is a new
	 * effective from date. If {@link #getEffectiveFromAsCalendar()} returns
	 * <code>null</code> the product component generation is not reset, for example
	 * if this model object was removed from its parent.
	 * <p>
	 * Clients may change the behavior of resetting the product component by
	 * overwriting {@link #resetProductCmptGenerationAfterEffectiveFromHasChanged()}
	 * instead of this method.
	 *
	 * @generated
	 */
	@IpsGenerated
	public void effectiveFromHasChanged() {
		if (getEffectiveFromAsCalendar() != null) {
			resetProductCmptGenerationAfterEffectiveFromHasChanged();
		}
	}

	/**
	 * Clears the product component generation.
	 * <p>
	 * This method can be overwritten to affect the behavior in case of an
	 * effective-date change.
	 *
	 * @generated
	 */
	@IpsGenerated
	protected void resetProductCmptGenerationAfterEffectiveFromHasChanged() {
		productConfiguration.resetProductCmptGeneration();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public Calendar getEffectiveFromAsCalendar() {
		IModelObject parent = getParentModelObject();
		if (parent instanceof IConfigurableModelObject) {
			return ((IConfigurableModelObject) parent).getEffectiveFromAsCalendar();
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IModelObject getParentModelObject() {
		if (kfzVertrag != null) {
			return kfzVertrag;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void initFromXml(Element objectEl, boolean initWithProductDefaultsBeforeReadingXmlData,
			IRuntimeRepository productRepository, IObjectReferenceStore store, XmlCallback xmlCallback,
			String currPath) {
		productConfiguration.initFromXml(objectEl, productRepository);
		if (initWithProductDefaultsBeforeReadingXmlData) {
			initialize();
		}
		super.initFromXml(objectEl, initWithProductDefaultsBeforeReadingXmlData, productRepository, store, xmlCallback,
				currPath);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void initPropertiesFromXml(Map<String, String> propMap, IRuntimeRepository productRepository) {
		super.initPropertiesFromXml(propMap, productRepository);
		doInitMarke(propMap);
		doInitPS(propMap);
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitMarke(Map<String, String> propMap) {
		if (propMap.containsKey(PROPERTY_MARKE)) {
			this.marke = IpsStringUtils.isEmpty(propMap.get(PROPERTY_MARKE)) ? null
					: FahrzeugMarken.getValueById(propMap.get(PROPERTY_MARKE));
		}
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitPS(Map<String, String> propMap) {
		if (propMap.containsKey(PROPERTY_PS)) {
			this.pS = IpsStringUtils.isEmpty(propMap.get(PROPERTY_PS)) ? null
					: Integer.valueOf(propMap.get(PROPERTY_PS));
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected AbstractModelObject createChildFromXml(Element childEl) {
		AbstractModelObject newChild = super.createChildFromXml(childEl);
		if (newChild != null) {
			return newChild;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected IUnresolvedReference createUnresolvedReference(Object objectId, String targetRole, String targetId)
			throws Exception {
		if ("HaftpflichtVersicherung".equals(targetRole)) {
			return new DefaultUnresolvedReference(this, objectId, "setHaftpflichtVersicherung",
					HaftpflichtVersicherung.class, targetId);
		}
		if ("KaskoVersicherung".equals(targetRole)) {
			return new DefaultUnresolvedReference(this, objectId, "setKaskoVersicherung", KaskoVersicherung.class,
					targetId);
		}
		return super.createUnresolvedReference(objectId, targetRole, targetId);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IModelObjectDelta computeDelta(IModelObject otherObject, IDeltaComputationOptions options) {
		ModelObjectDelta delta = ModelObjectDelta.newDelta(this, otherObject, options);
		if (!Fahrzeug.class.isAssignableFrom(otherObject.getClass())) {
			return delta;
		}
		Fahrzeug otherFahrzeug = (Fahrzeug) otherObject;
		delta.checkPropertyChange(Fahrzeug.PROPERTY_MARKE, marke, otherFahrzeug.marke, options);
		delta.checkPropertyChange(Fahrzeug.PROPERTY_PS, pS, otherFahrzeug.pS, options);
		if (!options.ignoreAssociations()) {
			ModelObjectDelta.createAssociatedChildDeltas(delta, haftpflichtVersicherung,
					otherFahrzeug.haftpflichtVersicherung, ASSOCIATION_HAFTPFLICHT_VERSICHERUNG, options);
			ModelObjectDelta.createAssociatedChildDeltas(delta, kaskoVersicherung, otherFahrzeug.kaskoVersicherung,
					ASSOCIATION_KASKO_VERSICHERUNG, options);
		}
		return delta;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public Fahrzeug newCopy() {
		Map<IModelObject, IModelObject> copyMap = new HashMap<>();
		Fahrzeug newCopy = newCopyInternal(copyMap);
		copyAssociationsInternal(newCopy, copyMap);
		return newCopy;
	}

	/**
	 * Internal copy method with a {@link Map} containing already copied instances.
	 * 
	 * @param copyMap the map contains the copied instances
	 *
	 * @generated
	 */
	@IpsGenerated
	public Fahrzeug newCopyInternal(Map<IModelObject, IModelObject> copyMap) {
		Fahrzeug newCopy = (Fahrzeug) copyMap.get(this);
		if (newCopy == null) {
			newCopy = new Fahrzeug();
			copyMap.put(this, newCopy);
			newCopy.copyProductCmptAndGenerationInternal(this);
			copyProperties(newCopy, copyMap);
		}
		return newCopy;
	}

	/**
	 * Copies the product component and product component generation from the other
	 * object.
	 *
	 * @generated
	 */
	@IpsGenerated
	protected void copyProductCmptAndGenerationInternal(Fahrzeug otherObject) {
		productConfiguration.copy(otherObject.productConfiguration);
	}

	/**
	 * This method sets all properties in the copy with the values of this object.
	 * If there are copied associated objects they are added to the copyMap in
	 * {@link #newCopyInternal(Map)}.
	 * 
	 * @param copy    The copy object
	 * @param copyMap a map containing copied associated objects
	 *
	 * @generated
	 */
	@IpsGenerated
	protected void copyProperties(IModelObject copy, Map<IModelObject, IModelObject> copyMap) {
		Fahrzeug concreteCopy = (Fahrzeug) copy;
		concreteCopy.marke = marke;
		concreteCopy.pS = pS;
		concreteCopy.haftpflichtVersicherung = haftpflichtVersicherung;
		concreteCopy.kaskoVersicherung = kaskoVersicherung;
	}

	/**
	 * Internal method for setting copied associations. For copied targets, the
	 * associations have to be retargeted to the new copied instances. This method
	 * have to call {@link #copyAssociationsInternal(IModelObject, Map)} in other
	 * instances associated by composite.
	 * 
	 * @param abstractCopy the copy of this policy component
	 * @param copyMap      the map contains the copied instances
	 *
	 * @generated
	 */
	@IpsGenerated
	public void copyAssociationsInternal(IModelObject abstractCopy, Map<IModelObject, IModelObject> copyMap) {
		Fahrzeug newCopy = (Fahrzeug) abstractCopy;
		if (copyMap.containsKey(haftpflichtVersicherung)) {
			newCopy.haftpflichtVersicherung = (HaftpflichtVersicherung) copyMap.get(haftpflichtVersicherung);
		}
		if (copyMap.containsKey(kaskoVersicherung)) {
			newCopy.kaskoVersicherung = (KaskoVersicherung) copyMap.get(kaskoVersicherung);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public boolean accept(IModelObjectVisitor visitor) {
		if (!visitor.visit(this)) {
			return false;
		}
		return true;
	}

	/**
	 * Validates the object (but not its children). Returns <code>true</code> if
	 * this object should continue validating, <code>false</code> otherwise.
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public boolean validateSelf(MessageList ml, IValidationContext context) {
		if (!super.validateSelf(ml, context)) {
			return STOP_VALIDATION;
		}
		if (!bmwNur200PsRegel(ml, context)) {
			return STOP_VALIDATION;
		}
		if (!vw400PsAnfragePflichtig(ml, context)) {
			return STOP_VALIDATION;
		}
		return CONTINUE_VALIDATION;
	}

	/**
	 * Validates the object's children.
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public void validateDependants(MessageList ml, IValidationContext context) {
		super.validateDependants(ml, context);
	}

	/**
	 * @restrainedmodifiable
	 */
	@Override
	@IpsGenerated
	public String toString() {
		// begin-user-code
		return getProductComponent() == null ? getClass().getSimpleName()
				: getClass().getSimpleName() + '[' + getProductComponent().toString() + ']';
		// end-user-code
	}

	/**
	 * Executes the rule bmwNur200PsRegel and adds a message to the given list if
	 * the object is invalid.
	 * 
	 * @param ml      list to which validation errors are added
	 * @param context the validation context
	 * @return <code>true</code>, if the validation should be continued,
	 *         <code>false</code> if it should be stopped after processing this
	 *         rule.
	 * @since 1.0.0
	 *
	 * @restrainedmodifiable
	 */
	@IpsValidationRule(name = "bmwNur200PsRegel", msgCode = MSG_CODE_BMW_NUR200_PS_REGEL, severity = Severity.ERROR)
	@IpsGenerated
	protected boolean bmwNur200PsRegel(MessageList ml, IValidationContext context) {
		// begin-user-code
		// TODO Implement the rule bmwNur200PsRegel.
		if (pS != null && pS > 200 && FahrzeugMarken.BMW.equals(getMarke())) {
			ml.add(createMessageForRuleBmwNur200PsRegel(context));
		}
		return CONTINUE_VALIDATION;
		// end-user-code
	}

	/**
	 * Creates a message to indicate that the rule bmwNur200PsRegel has found an
	 * invalid state.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	protected Message createMessageForRuleBmwNur200PsRegel(IValidationContext context) {
		List<ObjectProperty> invalidObjectProperties = Arrays.asList(new ObjectProperty(this, PROPERTY_MARKE));
		MessagesHelper messageHelper = new MessagesHelper("at.faktorzehn.kfz.model.internal.message-validation",
				getClass().getClassLoader(), Locale.GERMAN);
		String msgText = messageHelper.getMessage("Fahrzeug-bmwNur200PsRegel", context.getLocale());

		Message.Builder builder = new Message.Builder(msgText, Severity.ERROR).code(MSG_CODE_BMW_NUR200_PS_REGEL)
				.invalidObjects(invalidObjectProperties);
		return builder.create();
	}

	/**
	 * Executes the rule vw400PsAnfragePflichtig and adds a message to the given
	 * list if the object is invalid.
	 * 
	 * @param ml      list to which validation errors are added
	 * @param context the validation context
	 * @return <code>true</code>, if the validation should be continued,
	 *         <code>false</code> if it should be stopped after processing this
	 *         rule.
	 * @since 1.0.0
	 *
	 * @restrainedmodifiable
	 */
	@IpsValidationRule(name = "vw400PsAnfragePflichtig", msgCode = MSG_CODE_VW400_PS_ANFRAGE_PFLICHTIG, severity = Severity.WARNING)
	@IpsConfiguredValidationRule(changingOverTime = false, defaultActivated = true)
	@IpsGenerated
	protected boolean vw400PsAnfragePflichtig(MessageList ml, IValidationContext context) {
		if (getProductComponent().isValidationRuleActivated(RULE_VW400_PS_ANFRAGE_PFLICHTIG)) {
			// begin-user-code
			if (pS != null && pS > 400 && FahrzeugMarken.SKODA.equals(getMarke())) {
				ml.add(createMessageForRuleVw400PsAnfragePflichtig(context));
			}
			return CONTINUE_VALIDATION;
			// end-user-code
		}
		return CONTINUE_VALIDATION;
	}

	/**
	 * Creates a message to indicate that the rule vw400PsAnfragePflichtig has found
	 * an invalid state.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	protected Message createMessageForRuleVw400PsAnfragePflichtig(IValidationContext context) {
		MessagesHelper messageHelper = new MessagesHelper("at.faktorzehn.kfz.model.internal.message-validation",
				getClass().getClassLoader(), Locale.GERMAN);
		String msgText = messageHelper.getMessage("Fahrzeug-vw400PsAnfragePflichtig", context.getLocale());

		Message.Builder builder = new Message.Builder(msgText, Severity.WARNING)
				.code(MSG_CODE_VW400_PS_ANFRAGE_PFLICHTIG).invalidObject(new ObjectProperty(this));
		return builder.create();
	}

	/**
	 * Creates a new instance of FahrzeugBuilder to edit this policy.
	 *
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugBuilder modify() {
		return FahrzeugBuilder.from(this, getProductComponent().getRepository());
	}

	/**
	 * The runtime repository is used to create configured association targets from
	 * existing product components.
	 *
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugBuilder modify(IRuntimeRepository runtimeRepository) {
		return FahrzeugBuilder.from(this, runtimeRepository);
	}

	/**
	 * Creates a new FahrzeugBuilder with a new instance of policy. Runtime
	 * repository is set to null.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static FahrzeugBuilder builder() {
		return FahrzeugBuilder.from(new Fahrzeug(), null);
	}

	/**
	 * Creates a new FahrzeugBuilder with a new instance of policy. Runtime
	 * repository is set to null. The runtime repository is required as association
	 * targets exists that are configured by a product. The product components of
	 * the targets must be in the given runtime repository.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static FahrzeugBuilder builder(IRuntimeRepository runtimeRepository) {
		return FahrzeugBuilder.from(new Fahrzeug(), runtimeRepository);
	}

	/**
	 * Creates a new FahrzeugBuilder with a new instance of policy created by the
	 * given product component.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static FahrzeugBuilder builder(FahrzeugTyp productCmpt) {
		return FahrzeugBuilder.from(new Fahrzeug(productCmpt), productCmpt.getRepository());
	}

	/**
	 * Creates a new FahrzeugBuilder with a new instance of policy created by the
	 * product component with the given ID in the runtime repository that configures
	 * the policy.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static FahrzeugBuilder builder(IRuntimeRepository runtimeRepository, String productCmptId) {
		FahrzeugTyp product = (FahrzeugTyp) runtimeRepository.getProductComponent(productCmptId);
		if (product == null) {
			throw new RuntimeException("No product component found with given ID!");
		} else {
			Fahrzeug policy = product.createFahrzeug();

			policy.initialize();
			return FahrzeugBuilder.from(policy, runtimeRepository);
		}
	}

}
