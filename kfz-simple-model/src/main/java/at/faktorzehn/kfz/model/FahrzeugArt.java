package at.faktorzehn.kfz.model;

import org.faktorips.runtime.model.annotation.IpsDocumented;
import org.faktorips.runtime.model.annotation.IpsEnumType;
import java.util.Map;
import java.util.HashMap;
import org.faktorips.runtime.model.annotation.IpsEnumAttribute;
import org.faktorips.runtime.annotation.IpsGenerated;

/**
 * @since 1.0.0
 *
 * @generated
 */
@IpsDocumented(bundleName = "at.faktorzehn.kfz.model.model-label-and-descriptions", defaultLocale = "de")
@IpsEnumType(name = "FahrzeugArt", attributeNames = { "id", "name" })
public enum FahrzeugArt {
	/**
	 * @generated
	 */
	PKW("PKW", "PKW"),
	/**
	 * @generated
	 */
	MOTORRAD("MOTORRAD", "MOTORRAD")

	;

	/**
	 * This map is used to have high performance access to the values by ID.
	 *
	 * @generated
	 */
	private static final Map<String, FahrzeugArt> ID_MAP;
	/**
	 * In this static block the id map is initialized with all the values in this
	 * enum.
	 *
	 * @generated
	 */
	static {
		ID_MAP = new HashMap<>();
		for (FahrzeugArt value : values()) {
			ID_MAP.put(value.id, value);
		}
	}

	/**
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private final String id;
	/**
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private final String name;

	/**
	 * Creates a new instance of FahrzeugArt.
	 *
	 * @generated
	 */
	@IpsGenerated
	private FahrzeugArt(String id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Returns the enumeration value for the specified parameter <code>id</code>.
	 * Returns <code>null</code> if no corresponding enumeration value is found, or
	 * if the parameter <code>id</code> is <code>null</code>.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static final FahrzeugArt getValueById(String id) {
		return ID_MAP.get(id);
	}

	/**
	 * Returns the enumeration value for the specified parameter <code>name</code>.
	 * Returns <code>null</code> if no corresponding enumeration value is found, or
	 * if the parameter <code>name</code> is <code>null</code>.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static final FahrzeugArt getValueByName(String name) {
		for (FahrzeugArt currentValue : values()) {
			if (currentValue.name.equals(name)) {
				return currentValue;
			}
		}
		return null;
	}

	/**
	 * Returns the enumeration value for the specified parameter <code>id</code>. If
	 * no corresponding enum value is found for the given parameter, an
	 * {@link IllegalArgumentException} is thrown.
	 *
	 * @throws IllegalArgumentException if no corresponding enum value is found
	 *
	 * @generated
	 */
	@IpsGenerated
	public static final FahrzeugArt getExistingValueById(String id) {
		if (ID_MAP.containsKey(id)) {
			return ID_MAP.get(id);
		} else {
			throw new IllegalArgumentException("No enum value with id " + id);
		}
	}

	/**
	 * Returns the enumeration value for the specified parameter <code>name</code>.
	 * If no corresponding enum value is found for the given parameter, an
	 * {@link IllegalArgumentException} is thrown.
	 *
	 * @throws IllegalArgumentException if no corresponding enum value is found
	 *
	 * @generated
	 */
	@IpsGenerated
	public static final FahrzeugArt getExistingValueByName(String name) {
		for (FahrzeugArt currentValue : values()) {
			if (currentValue.name.equals(name)) {
				return currentValue;
			}
		}
		throw new IllegalArgumentException("No enum value with name " + name);
	}

	/**
	 * Returns <code>true</code> if the provided parameter value identifies a value
	 * of this enumeration.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static final boolean isValueById(String id) {
		return getValueById(id) != null;
	}

	/**
	 * Returns <code>true</code> if the provided parameter value identifies a value
	 * of this enumeration.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static final boolean isValueByName(String name) {
		return getValueByName(name) != null;
	}

	/**
	 * Returns the value of the attribute id.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsEnumAttribute(name = "id", identifier = true, unique = true)
	@IpsGenerated
	public String getId() {
		return id;
	}

	/**
	 * Returns the value of the attribute name.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsEnumAttribute(name = "name", unique = true, displayName = true)
	@IpsGenerated
	public String getName() {
		return name;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public String toString() {
		return "FahrzeugArt: " + id + '(' + name + ')';
	}
}
