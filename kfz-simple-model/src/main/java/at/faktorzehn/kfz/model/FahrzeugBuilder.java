package at.faktorzehn.kfz.model;

import org.faktorips.runtime.IRuntimeRepository;
import org.faktorips.runtime.annotation.IpsGenerated;

/**
 * This class provides methods to create and edit a Fahrzeug.
 *
 * @generated
 */
public class FahrzeugBuilder {
	/**
	 * @generated
	 */
	private final Fahrzeug fahrzeug;

	/**
	 * @generated
	 */
	private IRuntimeRepository runtimeRepository;

	/**
	 * Creates a new FahrzeugBuilder with a given policy and runtime repository.
	 * This constructor is only to be used internally by the builder!
	 *
	 * @generated
	 */
	@IpsGenerated
	protected FahrzeugBuilder(Fahrzeug policy, IRuntimeRepository runtimeRepository) {
		this.fahrzeug = policy;
		this.runtimeRepository = runtimeRepository;
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugBuilder setRepository(IRuntimeRepository runtimeRepository) {
		this.runtimeRepository = runtimeRepository;
		return this;
	}

	/**
	 * Sets the value of attribute marke.
	 *
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugBuilder marke(FahrzeugMarken newMarke) {
		fahrzeug.setMarke(newMarke);
		return this;
	}

	/**
	 * Sets the value of attribute PS.
	 *
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugBuilder pS(Integer newPS) {
		fahrzeug.setPS(newPS);
		return this;
	}

	/**
	 * Returns the Fahrzeug that is built by this instance.
	 *
	 * @generated
	 */
	@IpsGenerated
	public Fahrzeug getResult() {
		return fahrzeug;
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	public IRuntimeRepository getRepository() {
		return runtimeRepository;
	}

	/**
	 * 
	 *
	 * @generated
	 */
	@IpsGenerated
	public static Class<?> getPolicyClass() {
		return Fahrzeug.class;
	}

	/**
	 * Creates a new FahrzeugBuilder with a given Fahrzeug.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static FahrzeugBuilder from(Fahrzeug policy) {
		return new FahrzeugBuilder(policy, null);
	}

	/**
	 * Creates a new FahrzeugBuilder with a given Fahrzeug and a runtime repository.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static FahrzeugBuilder from(Fahrzeug policy, IRuntimeRepository runtimeRepository) {
		return new FahrzeugBuilder(policy, runtimeRepository);
	}

	/**
	 * Returns an {@link AssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AssociationBuilder} you
	 * get the builder of the target object for further processing. Use the method
	 * {@link #add()} if you want to create multiple target objects and always
	 * return to this builder instead of the target builder.
	 * 
	 * @see #add()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AssociationBuilder associate() {
		return new AssociationBuilder(this);
	}

	/**
	 * Returns an {@link AddAssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AddAssociationBuilder}
	 * you always could return to this builder for further processing. Use the
	 * method {@link #associate()} if you want to have the builder of the target
	 * object.
	 * 
	 * @see #associate()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AddAssociationBuilder add() {
		return new AddAssociationBuilder(this);
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * returns a builder for the target class.
	 *
	 * @generated
	 */
	public static class AssociationBuilder {

		/**
		 * @generated
		 */
		private FahrzeugBuilder policyBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AssociationBuilder(FahrzeugBuilder policyBuilder) {
			this.policyBuilder = policyBuilder;
		}

		/**
		 * Creates a new instance of a subclass of HaftpflichtVersicherung with the
		 * given builder and sets it as the target of the association
		 * HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends HaftpflichtVersicherungBuilder> T haftpflichtVersicherung(T targetBuilder) {
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of HaftpflichtVersicherung and set it as the target of
		 * the association HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungBuilder haftpflichtVersicherung() {
			HaftpflichtVersicherungBuilder targetBuilder = HaftpflichtVersicherung.builder();
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of HaftpflichtVersicherung from a given product
		 * component and sets it as the target of the association
		 * HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungBuilder haftpflichtVersicherung(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			HaftpflichtVersicherungBuilder targetBuilder = null;
			targetBuilder = HaftpflichtVersicherung.builder(getRepository(), productCmptId);
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of a subclass of KaskoVersicherung with the given
		 * builder and sets it as the target of the association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends KaskoVersicherungBuilder> T kaskoVersicherung(T targetBuilder) {
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of KaskoVersicherung and set it as the target of the
		 * association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungBuilder kaskoVersicherung() {
			KaskoVersicherungBuilder targetBuilder = KaskoVersicherung.builder();
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of KaskoVersicherung from a given product component
		 * and sets it as the target of the association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungBuilder kaskoVersicherung(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			KaskoVersicherungBuilder targetBuilder = null;
			targetBuilder = KaskoVersicherung.builder(getRepository(), productCmptId);
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected Fahrzeug getResult() {
			return policyBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected IRuntimeRepository getRepository() {
			return policyBuilder.getRepository();
		}
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * return the original FahrzeugBuilder.
	 *
	 * @generated
	 */
	public static class AddAssociationBuilder {

		/**
		 * @generated
		 */
		private FahrzeugBuilder policyBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AddAssociationBuilder(FahrzeugBuilder policyBuilder) {
			this.policyBuilder = policyBuilder;
		}

		/**
		 * Set a existing instance of HaftpflichtVersicherung as the target of the
		 * association HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder haftpflichtVersicherung(HaftpflichtVersicherung targetPolicy) {
			getResult().setHaftpflichtVersicherung(targetPolicy);
			return done();
		}

		/**
		 * Creates a new instance of a subclass of HaftpflichtVersicherung with the
		 * given builder and sets it as the target of the association
		 * HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder haftpflichtVersicherung(HaftpflichtVersicherungBuilder targetBuilder) {
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of HaftpflichtVersicherung and set it as the target of
		 * the association HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder haftpflichtVersicherung() {
			HaftpflichtVersicherungBuilder targetBuilder = HaftpflichtVersicherung.builder();
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of HaftpflichtVersicherung from a given product
		 * component and sets it as the target of the association
		 * HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder haftpflichtVersicherung(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			HaftpflichtVersicherungBuilder targetBuilder = null;
			targetBuilder = HaftpflichtVersicherung.builder(getRepository(), productCmptId);
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * Set a existing instance of KaskoVersicherung as the target of the association
		 * KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder kaskoVersicherung(KaskoVersicherung targetPolicy) {
			getResult().setKaskoVersicherung(targetPolicy);
			return done();
		}

		/**
		 * Creates a new instance of a subclass of KaskoVersicherung with the given
		 * builder and sets it as the target of the association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder kaskoVersicherung(KaskoVersicherungBuilder targetBuilder) {
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of KaskoVersicherung and set it as the target of the
		 * association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder kaskoVersicherung() {
			KaskoVersicherungBuilder targetBuilder = KaskoVersicherung.builder();
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of KaskoVersicherung from a given product component
		 * and sets it as the target of the association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder kaskoVersicherung(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			KaskoVersicherungBuilder targetBuilder = null;
			targetBuilder = KaskoVersicherung.builder(getRepository(), productCmptId);
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected FahrzeugBuilder done() {
			return policyBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected Fahrzeug getResult() {
			return policyBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected IRuntimeRepository getRepository() {
			return policyBuilder.getRepository();
		}
	}

}
