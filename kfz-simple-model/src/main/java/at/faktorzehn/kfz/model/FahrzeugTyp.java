package at.faktorzehn.kfz.model;

import org.faktorips.runtime.model.annotation.IpsProductCmptType;
import org.faktorips.runtime.model.annotation.IpsAttributes;
import org.faktorips.runtime.model.annotation.IpsAssociations;
import org.faktorips.runtime.model.annotation.IpsConfigures;
import org.faktorips.runtime.model.annotation.IpsDocumented;
import org.faktorips.runtime.internal.ProductComponent;
import org.faktorips.runtime.IProductComponentLink;
import org.faktorips.runtime.IRuntimeRepository;
import org.faktorips.runtime.model.annotation.IpsAttribute;
import org.faktorips.runtime.model.type.AttributeKind;
import org.faktorips.runtime.model.type.ValueSetKind;
import org.faktorips.runtime.model.annotation.IpsAttributeSetter;
import org.faktorips.runtime.IllegalRepositoryModificationException;
import org.faktorips.runtime.model.annotation.IpsAssociation;
import org.faktorips.runtime.model.type.AssociationKind;
import org.faktorips.runtime.model.annotation.IpsAssociationAdder;
import org.faktorips.runtime.internal.ProductComponentLink;
import org.faktorips.runtime.model.annotation.IpsAssociationLinks;
import org.w3c.dom.Element;
import java.util.Map;
import org.faktorips.runtime.internal.ValueToXmlHelper;
import org.faktorips.runtime.internal.IpsStringUtils;
import org.faktorips.runtime.internal.MultiValueXmlHelper;
import java.util.List;
import org.faktorips.runtime.IProductComponent;
import java.util.ArrayList;
import org.faktorips.runtime.InMemoryRuntimeRepository;
import org.faktorips.runtime.internal.DateTime;
import org.faktorips.runtime.annotation.IpsGenerated;

/**
 * Implementation for FahrzeugTyp.
 * 
 * @since 1.0.0
 *
 * @generated
 */
@IpsProductCmptType(name = "FahrzeugTyp")
@IpsAttributes({ "art", "marken" })
@IpsAssociations({ "KfzProdukt" })
@IpsConfigures(Fahrzeug.class)
@IpsDocumented(bundleName = "at.faktorzehn.kfz.model.model-label-and-descriptions", defaultLocale = "de")
public class FahrzeugTyp extends ProductComponent {

	/**
	 * The name of the XML tag for the association kfzProdukt.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private static final String XML_TAG_KFZ_PRODUKT = "KfzProdukt";
	/**
	 * The name of the property art.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_ART = "art";
	/**
	 * The name of the property marken.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_MARKEN = "marken";
	/**
	 * The product component property Art.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private FahrzeugArt art;

	/**
	 * The product component property Marken.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private List<FahrzeugMarken> marken;
	/**
	 * Member variable for the association KfzProdukt.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private IProductComponentLink<KfzProdukt> kfzProdukt = null;

	/**
	 * Creates a new FahrzeugTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugTyp(IRuntimeRepository repository, String id, String kindId, String versionId) {
		super(repository, id, kindId, versionId);
		setMarkenInternal(new ArrayList<>());
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public boolean isChangingOverTime() {
		return false;
	}

	/**
	 * Returns the value of art.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttribute(name = "art", kind = AttributeKind.CONSTANT, valueSetKind = ValueSetKind.Enum)
	@IpsGenerated
	public FahrzeugArt getArt() {
		return art;
	}

	/**
	 * Sets the value of attribute art.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttributeSetter("art")
	@IpsGenerated
	public void setArt(FahrzeugArt newValue) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		setArtInternal(newValue);
	}

	/**
	 * Sets the value of attribute art.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	protected final void setArtInternal(FahrzeugArt newValue) {
		this.art = newValue;
	}

	/**
	 * Returns the value of marken.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttribute(name = "marken", kind = AttributeKind.CONSTANT, valueSetKind = ValueSetKind.AllValues)
	@IpsGenerated
	public List<FahrzeugMarken> getMarken() {
		return new ArrayList<>(marken);
	}

	/**
	 * Sets the value of attribute marken.
	 * 
	 * @since 1.0.0
	 * @generated
	 */
	@IpsAttributeSetter("marken")
	@IpsGenerated
	public void setMarken(List<FahrzeugMarken> newValue) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		setMarkenInternal(new ArrayList<>(newValue));
	}

	/**
	 * Sets the value of attribute marken.
	 * 
	 * @since 1.0.0
	 * @generated
	 */
	@IpsGenerated
	protected final void setMarkenInternal(List<FahrzeugMarken> newValue) {
		this.marken = newValue;
	}

	/**
	 * Returns the referenced KfzProdukt or <code>null</code> if there is no
	 * referenced KfzProdukt.
	 * 
	 * @throws org.faktorips.runtime.ProductCmptNotFoundException if a product
	 *                                                            component is
	 *                                                            referenced but can
	 *                                                            not be found.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "KfzProdukt", pluralName = "KfzProdukte", kind = AssociationKind.Composition, targetClass = KfzProdukt.class, min = 0, max = 1)
	@IpsGenerated
	public KfzProdukt getKfzProdukt() {
		return kfzProdukt != null ? kfzProdukt.getTarget() : null;
	}

	/**
	 * Sets the new KfzProdukt.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "KfzProdukt")
	@IpsGenerated
	public void setKfzProdukt(KfzProdukt target) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		kfzProdukt = (target == null ? null : new ProductComponentLink<>(this, target, "KfzProdukt"));
	}

	/**
	 * Returns the <code>ILink</code> to the KfzProdukt or <code>null</code>, if no
	 * object is referenced.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationLinks(association = "KfzProdukt")
	@IpsGenerated
	public IProductComponentLink<KfzProdukt> getLinkForKfzProdukt() {
		return kfzProdukt;
	}

	/**
	 * Returns the <code>ILink</code> to the KfzProdukt at the indicated index.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public IProductComponentLink<KfzProdukt> getLinkForKfzProdukt(KfzProdukt productComponent) {
		return kfzProdukt != null && kfzProdukt.getTargetId().equals(productComponent.getId()) ? kfzProdukt : null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void doInitPropertiesFromXml(Map<String, Element> configMap) {
		super.doInitPropertiesFromXml(configMap);
		doInitArt(configMap);
		doInitMarken(configMap);
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitArt(Map<String, Element> configMap) {
		Element configElement = configMap.get(PROPERTY_ART);
		if (configElement != null) {
			String value = ValueToXmlHelper.getValueFromElement(configElement, ValueToXmlHelper.XML_TAG_VALUE);
			this.art = IpsStringUtils.isEmpty(value) ? null : FahrzeugArt.getValueById(value);
		}
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitMarken(Map<String, Element> configMap) {
		Element configElement = configMap.get(PROPERTY_MARKEN);
		if (configElement != null) {
			List<FahrzeugMarken> valueList = new ArrayList<>();
			List<String> stringList = MultiValueXmlHelper.getValuesFromXML(configElement);
			for (String stringValue : stringList) {
				FahrzeugMarken convertedValue = IpsStringUtils.isEmpty(stringValue) ? null
						: FahrzeugMarken.getValueById(stringValue);
				valueList.add(convertedValue);
			}
			this.marken = valueList;
		}
	}

	/**
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void doInitReferencesFromXml(Map<String, List<Element>> elementsMap) {
		super.doInitReferencesFromXml(elementsMap);
		doInitKfzProdukt(elementsMap);
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitKfzProdukt(Map<String, List<Element>> elementsMap) {
		List<Element> associationElements = elementsMap.get(XML_TAG_KFZ_PRODUKT);
		if (associationElements != null) {
			Element element = associationElements.get(0);
			kfzProdukt = new ProductComponentLink<>(this);
			kfzProdukt.initFromXml(element);
		}
	}

	/**
	 * Creates a new Fahrzeug that is configured.
	 *
	 * @generated
	 */
	@IpsGenerated
	public Fahrzeug createFahrzeug() {
		Fahrzeug policy = new Fahrzeug(this);
		policy.initialize();
		return policy;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public Fahrzeug createPolicyComponent() {
		return createFahrzeug();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IProductComponentLink<? extends IProductComponent> getLink(String linkName, IProductComponent target) {
		if ("KfzProdukt".equals(linkName)) {
			return getLinkForKfzProdukt((KfzProdukt) target);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public List<IProductComponentLink<? extends IProductComponent>> getLinks() {
		List<IProductComponentLink<? extends IProductComponent>> list = new ArrayList<>();
		if (getLinkForKfzProdukt() != null) {
			list.add(getLinkForKfzProdukt());
		}
		return list;
	}

	/**
	 * Creates a new instance of FahrzeugTypBuilder to edit this product.
	 *
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugTypBuilder modify() {
		return FahrzeugTypBuilder.from(this, (InMemoryRuntimeRepository) this.getRepository());
	}

	/**
	 * Generates a new instance of FahrzeugTyp with a given
	 * {@link InMemoryRuntimeRepository}, ID, kindID and versionID. The new product
	 * is set to be valid from 1900/1/1.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static FahrzeugTypBuilder builder(InMemoryRuntimeRepository runtimeRepository, String id, String kindId,
			String versionId) {
		FahrzeugTyp product = new FahrzeugTyp(runtimeRepository, id, kindId, versionId);
		product.setValidFrom(new DateTime(1900, 1, 1));
		runtimeRepository.putProductComponent(product);

		return new FahrzeugTypBuilder(product, runtimeRepository);
	}

	/**
	 * Generates a new instance of FahrzeugTyp with a given
	 * {@link InMemoryRuntimeRepository}, ID, kindID and versionID.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static FahrzeugTypBuilder builder(InMemoryRuntimeRepository runtimeRepository, String id, String kindId,
			String versionId, DateTime validFrom) {
		FahrzeugTyp product = new FahrzeugTyp(runtimeRepository, id, kindId, versionId);
		product.setValidFrom(validFrom);
		runtimeRepository.putProductComponent(product);

		return new FahrzeugTypBuilder(product, runtimeRepository);
	}

	/**
	 * Generates a new instance of FahrzeugTyp with the ID of an existing product
	 * component.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static FahrzeugTypBuilder builder(InMemoryRuntimeRepository runtimeRepository, String prodCmptId) {
		FahrzeugTyp product = (FahrzeugTyp) runtimeRepository.getProductComponent(prodCmptId);

		if (product == null) {
			throw new RuntimeException("No product component found with given ID!");
		} else {
			return FahrzeugTypBuilder.from(product, runtimeRepository);
		}
	}
}
