package at.faktorzehn.kfz.model;

import org.faktorips.runtime.InMemoryRuntimeRepository;
import java.util.List;
import org.faktorips.runtime.annotation.IpsGenerated;

/**
 * Implementation of FahrzeugTypBuilder. A FahrzeugTypBuilder provides all
 * functionalities that is needed to create a FahrzeugTyp. Note that this is
 * only for testing purpose. All instances of product have to be created in a
 * new {@link InMemoryRuntimeRepository} as Product Component in the runtime
 * repository can not be edited.
 *
 * @generated
 */
public class FahrzeugTypBuilder {
	/**
	 * @generated
	 */
	private final InMemoryRuntimeRepository runtimeRepository;

	/**
	 * @generated
	 */
	private final FahrzeugTyp fahrzeugTyp;

	/**
	 * Generates a new instance of FahrzeugTypBuilder with a given product. The
	 * product must exist in the given {@link InMemoryRuntimeRepository} that is not
	 * allowed to be null.
	 *
	 * @generated
	 */
	@IpsGenerated
	protected FahrzeugTypBuilder(FahrzeugTyp product, InMemoryRuntimeRepository runtimeRepository) {
		if (product == null || runtimeRepository == null) {
			throw new RuntimeException("Product and repository can not be null!");
		} else {
			runtimeRepository.getExistingProductComponent(product.getId());

			this.runtimeRepository = runtimeRepository;
			this.fahrzeugTyp = product;
		}
	}

	/**
	 * Sets the value of attribute art.
	 *
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugTypBuilder art(FahrzeugArt art) {
		fahrzeugTyp.setArt(art);
		return this;
	}

	/**
	 * Sets the value of attribute marken.
	 * 
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugTypBuilder marken(List<FahrzeugMarken> marken) {
		fahrzeugTyp.setMarken(marken);
		return this;
	}

	/**
	 * @return {@link InMemoryRuntimeRepository} that is saved.
	 *
	 * @generated
	 */
	@IpsGenerated
	public InMemoryRuntimeRepository getRepository() {
		return this.runtimeRepository;
	}

	/**
	 * @return instance of FahrzeugTyp that is built.
	 *
	 * @generated
	 */
	@IpsGenerated
	public FahrzeugTyp getResult() {
		return fahrzeugTyp;
	}

	/**
	 * Interne Methode
	 *
	 * @generated
	 */
	@IpsGenerated
	public static Class<?> getProductClass() {
		return FahrzeugTyp.class;
	}

	/**
	 * Creates a new FahrzeugTypBuilder with a given FahrzeugTyp and a runtime
	 * repository.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static FahrzeugTypBuilder from(FahrzeugTyp product, InMemoryRuntimeRepository runtimeRepository) {
		return new FahrzeugTypBuilder(product, runtimeRepository);
	}

	/**
	 * Returns an {@link AssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AssociationBuilder} you
	 * get the builder of the target object for further processing. Use the method
	 * {@link #add()} if you want to create multiple target objects and always
	 * return to this builder instead of the target builder.
	 * 
	 * @see #add()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AssociationBuilder associate() {
		return new AssociationBuilder(this);
	}

	/**
	 * Returns an {@link AddAssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AddAssociationBuilder}
	 * you always could return to this builder for further processing. Use the
	 * method {@link #associate()} if you want to have the builder of the target
	 * object.
	 * 
	 * @see #associate()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AddAssociationBuilder add() {
		return new AddAssociationBuilder(this);
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * returns a builder for the target class.
	 *
	 * @generated
	 */
	public static class AssociationBuilder {

		/**
		 * @generated
		 */
		private FahrzeugTypBuilder productBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AssociationBuilder(FahrzeugTypBuilder productBuilder) {
			this.productBuilder = productBuilder;
		}

		/**
		 * Gets a instance of a subclass of KfzProdukt with the ID of the product
		 * component and sets it as KfzProdukt.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends KfzProduktBuilder> T kfzProdukt(T targetBuilder) {
			getResult().setKfzProdukt(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Gets a instance of KfzProdukt with the ID of the product component and sets
		 * it as KfzProdukt.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder kfzProdukt(String productCmptId) {
			KfzProduktBuilder targetBuilder = KfzProdukt.builder(getRepository(), productCmptId);
			getResult().setKfzProdukt(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Generates a new Instance of KfzProdukt with the IDs and sets it as
		 * KfzProdukt.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder kfzProdukt(String id, String kindId, String versionId) {
			KfzProduktBuilder targetBuilder = KfzProdukt.builder(getRepository(), id, kindId, versionId);
			getResult().setKfzProdukt(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected FahrzeugTyp getResult() {
			return productBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected InMemoryRuntimeRepository getRepository() {
			return productBuilder.getRepository();
		}
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * return the original FahrzeugTypBuilder.
	 *
	 * @generated
	 */
	public static class AddAssociationBuilder {

		/**
		 * @generated
		 */
		private FahrzeugTypBuilder productBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AddAssociationBuilder(FahrzeugTypBuilder productBuilder) {
			this.productBuilder = productBuilder;
		}

		/**
		 * Sets a existing instance of KfzProdukt as the target of the association
		 * KfzProdukt.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder kfzProdukt(KfzProdukt targetProduct) {
			getResult().setKfzProdukt(targetProduct);
			return done();
		}

		/**
		 * Gets a instance of a subclass of KfzProdukt with the ID of the product
		 * component and sets it as KfzProdukt.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder kfzProdukt(KfzProduktBuilder targetBuilder) {
			getResult().setKfzProdukt(targetBuilder.getResult());
			return done();
		}

		/**
		 * Gets a instance of KfzProdukt with the ID of the product component and sets
		 * it as KfzProdukt.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder kfzProdukt(String productCmptId) {
			KfzProduktBuilder targetBuilder = KfzProdukt.builder(getRepository(), productCmptId);
			getResult().setKfzProdukt(targetBuilder.getResult());
			return done();
		}

		/**
		 * Generates a new Instance of KfzProdukt with the IDs and sets it as
		 * KfzProdukt.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder kfzProdukt(String id, String kindId, String versionId) {
			KfzProduktBuilder targetBuilder = KfzProdukt.builder(getRepository(), id, kindId, versionId);
			getResult().setKfzProdukt(targetBuilder.getResult());
			return done();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected FahrzeugTypBuilder done() {
			return productBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected FahrzeugTyp getResult() {
			return productBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected InMemoryRuntimeRepository getRepository() {
			return productBuilder.getRepository();
		}
	}

}
