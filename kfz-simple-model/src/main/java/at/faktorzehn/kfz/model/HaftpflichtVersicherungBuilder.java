package at.faktorzehn.kfz.model;

import org.faktorips.runtime.IRuntimeRepository;
import org.faktorips.runtime.annotation.IpsGenerated;

/**
 * This class provides methods to create and edit a HaftpflichtVersicherung.
 *
 * @generated
 */
public class HaftpflichtVersicherungBuilder {
	/**
	 * @generated
	 */
	private final HaftpflichtVersicherung haftpflichtVersicherung;

	/**
	 * @generated
	 */
	private IRuntimeRepository runtimeRepository;

	/**
	 * Creates a new HaftpflichtVersicherungBuilder with a given policy and runtime
	 * repository. This constructor is only to be used internally by the builder!
	 *
	 * @generated
	 */
	@IpsGenerated
	protected HaftpflichtVersicherungBuilder(HaftpflichtVersicherung policy, IRuntimeRepository runtimeRepository) {
		this.haftpflichtVersicherung = policy;
		this.runtimeRepository = runtimeRepository;
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	public HaftpflichtVersicherungBuilder setRepository(IRuntimeRepository runtimeRepository) {
		this.runtimeRepository = runtimeRepository;
		return this;
	}

	/**
	 * Returns the HaftpflichtVersicherung that is built by this instance.
	 *
	 * @generated
	 */
	@IpsGenerated
	public HaftpflichtVersicherung getResult() {
		return haftpflichtVersicherung;
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	public IRuntimeRepository getRepository() {
		return runtimeRepository;
	}

	/**
	 * 
	 *
	 * @generated
	 */
	@IpsGenerated
	public static Class<?> getPolicyClass() {
		return HaftpflichtVersicherung.class;
	}

	/**
	 * Creates a new HaftpflichtVersicherungBuilder with a given
	 * HaftpflichtVersicherung.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static HaftpflichtVersicherungBuilder from(HaftpflichtVersicherung policy) {
		return new HaftpflichtVersicherungBuilder(policy, null);
	}

	/**
	 * Creates a new HaftpflichtVersicherungBuilder with a given
	 * HaftpflichtVersicherung and a runtime repository.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static HaftpflichtVersicherungBuilder from(HaftpflichtVersicherung policy,
			IRuntimeRepository runtimeRepository) {
		return new HaftpflichtVersicherungBuilder(policy, runtimeRepository);
	}

	/**
	 * Returns an {@link AssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AssociationBuilder} you
	 * get the builder of the target object for further processing. Use the method
	 * {@link #add()} if you want to create multiple target objects and always
	 * return to this builder instead of the target builder.
	 * 
	 * @see #add()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AssociationBuilder associate() {
		return new AssociationBuilder(this);
	}

	/**
	 * Returns an {@link AddAssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AddAssociationBuilder}
	 * you always could return to this builder for further processing. Use the
	 * method {@link #associate()} if you want to have the builder of the target
	 * object.
	 * 
	 * @see #associate()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AddAssociationBuilder add() {
		return new AddAssociationBuilder(this);
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * returns a builder for the target class.
	 *
	 * @generated
	 */
	public static class AssociationBuilder {

		/**
		 * @generated
		 */
		private HaftpflichtVersicherungBuilder policyBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AssociationBuilder(HaftpflichtVersicherungBuilder policyBuilder) {
			this.policyBuilder = policyBuilder;
		}

		/**
		 * Creates a new instance of a subclass of Fahrzeug with the given builder and
		 * sets it as the target of the association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends FahrzeugBuilder> T fahrzeug(T targetBuilder) {
			getResult().setFahrzeug(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of Fahrzeug and set it as the target of the
		 * association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder fahrzeug() {
			FahrzeugBuilder targetBuilder = Fahrzeug.builder();
			getResult().setFahrzeug(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of Fahrzeug from a given product component and sets it
		 * as the target of the association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder fahrzeug(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			FahrzeugBuilder targetBuilder = null;
			targetBuilder = Fahrzeug.builder(getRepository(), productCmptId);
			getResult().setFahrzeug(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected HaftpflichtVersicherung getResult() {
			return policyBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected IRuntimeRepository getRepository() {
			return policyBuilder.getRepository();
		}
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * return the original HaftpflichtVersicherungBuilder.
	 *
	 * @generated
	 */
	public static class AddAssociationBuilder {

		/**
		 * @generated
		 */
		private HaftpflichtVersicherungBuilder policyBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AddAssociationBuilder(HaftpflichtVersicherungBuilder policyBuilder) {
			this.policyBuilder = policyBuilder;
		}

		/**
		 * Set a existing instance of Fahrzeug as the target of the association
		 * Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungBuilder fahrzeug(Fahrzeug targetPolicy) {
			getResult().setFahrzeug(targetPolicy);
			return done();
		}

		/**
		 * Creates a new instance of a subclass of Fahrzeug with the given builder and
		 * sets it as the target of the association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungBuilder fahrzeug(FahrzeugBuilder targetBuilder) {
			getResult().setFahrzeug(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of Fahrzeug and set it as the target of the
		 * association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungBuilder fahrzeug() {
			FahrzeugBuilder targetBuilder = Fahrzeug.builder();
			getResult().setFahrzeug(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of Fahrzeug from a given product component and sets it
		 * as the target of the association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungBuilder fahrzeug(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			FahrzeugBuilder targetBuilder = null;
			targetBuilder = Fahrzeug.builder(getRepository(), productCmptId);
			getResult().setFahrzeug(targetBuilder.getResult());
			return done();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected HaftpflichtVersicherungBuilder done() {
			return policyBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected HaftpflichtVersicherung getResult() {
			return policyBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected IRuntimeRepository getRepository() {
			return policyBuilder.getRepository();
		}
	}

}
