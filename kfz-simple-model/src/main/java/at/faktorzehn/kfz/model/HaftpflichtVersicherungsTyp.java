package at.faktorzehn.kfz.model;

import org.faktorips.runtime.model.annotation.IpsProductCmptType;
import org.faktorips.runtime.model.annotation.IpsAssociations;
import org.faktorips.runtime.model.annotation.IpsConfigures;
import org.faktorips.runtime.model.annotation.IpsDocumented;
import org.faktorips.runtime.internal.ProductComponent;
import org.faktorips.runtime.IProductComponentLink;
import org.faktorips.runtime.IRuntimeRepository;
import org.faktorips.runtime.model.annotation.IpsAssociation;
import org.faktorips.runtime.model.type.AssociationKind;
import org.faktorips.runtime.model.annotation.IpsMatchingAssociation;
import org.faktorips.runtime.model.annotation.IpsAssociationAdder;
import org.faktorips.runtime.IllegalRepositoryModificationException;
import org.faktorips.runtime.internal.ProductComponentLink;
import org.faktorips.runtime.CardinalityRange;
import org.faktorips.runtime.model.annotation.IpsAssociationLinks;
import org.w3c.dom.Element;
import java.util.Map;
import java.util.List;
import org.faktorips.runtime.IProductComponent;
import java.util.ArrayList;
import org.faktorips.runtime.InMemoryRuntimeRepository;
import org.faktorips.runtime.internal.DateTime;
import org.faktorips.runtime.annotation.IpsGenerated;

/**
 * Implementation for HaftpflichtVersicherungsTyp.
 * 
 * @since 1.0.0
 *
 * @generated
 */
@IpsProductCmptType(name = "HaftpflichtVersicherungsTyp")
@IpsAssociations({ "FahrzeugTyp" })
@IpsConfigures(HaftpflichtVersicherung.class)
@IpsDocumented(bundleName = "at.faktorzehn.kfz.model.model-label-and-descriptions", defaultLocale = "de")
public class HaftpflichtVersicherungsTyp extends ProductComponent {

	/**
	 * The name of the XML tag for the association fahrzeugTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private static final String XML_TAG_FAHRZEUG_TYP = "FahrzeugTyp";
	/**
	 * Member variable for the association FahrzeugTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private IProductComponentLink<FahrzeugTyp> fahrzeugTyp = null;

	/**
	 * Creates a new HaftpflichtVersicherungsTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public HaftpflichtVersicherungsTyp(IRuntimeRepository repository, String id, String kindId, String versionId) {
		super(repository, id, kindId, versionId);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public boolean isChangingOverTime() {
		return false;
	}

	/**
	 * Returns the referenced FahrzeugTyp or <code>null</code> if there is no
	 * referenced FahrzeugTyp.
	 * 
	 * @throws org.faktorips.runtime.ProductCmptNotFoundException if a product
	 *                                                            component is
	 *                                                            referenced but can
	 *                                                            not be found.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "FahrzeugTyp", pluralName = "FahrzeugTypen", kind = AssociationKind.Association, targetClass = FahrzeugTyp.class, min = 0, max = 1)
	@IpsMatchingAssociation(source = HaftpflichtVersicherung.class, name = "Fahrzeug")
	@IpsGenerated
	public FahrzeugTyp getFahrzeugTyp() {
		return fahrzeugTyp != null ? fahrzeugTyp.getTarget() : null;
	}

	/**
	 * Sets the new FahrzeugTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "FahrzeugTyp")
	@IpsGenerated
	public void setFahrzeugTyp(FahrzeugTyp target) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		fahrzeugTyp = (target == null ? null : new ProductComponentLink<>(this, target, "FahrzeugTyp"));
	}

	/**
	 * Sets the new FahrzeugTyp with the given cardinality.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "FahrzeugTyp", withCardinality = true)
	@IpsGenerated
	public void setFahrzeugTyp(FahrzeugTyp target, CardinalityRange cardinality) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		fahrzeugTyp = (target == null ? null : new ProductComponentLink<>(this, target, cardinality, "FahrzeugTyp"));
	}

	/**
	 * Returns the <code>ILink</code> to the FahrzeugTyp or <code>null</code>, if no
	 * object is referenced.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationLinks(association = "FahrzeugTyp")
	@IpsGenerated
	public IProductComponentLink<FahrzeugTyp> getLinkForFahrzeugTyp() {
		return fahrzeugTyp;
	}

	/**
	 * Returns the <code>ILink</code> to the FahrzeugTyp at the indicated index.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public IProductComponentLink<FahrzeugTyp> getLinkForFahrzeugTyp(FahrzeugTyp productComponent) {
		return fahrzeugTyp != null && fahrzeugTyp.getTargetId().equals(productComponent.getId()) ? fahrzeugTyp : null;
	}

	/**
	 * Returns the cardinality for the number of allowed instanced for Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public CardinalityRange getCardinalityForFahrzeug(FahrzeugTyp productCmpt) {
		if (productCmpt != null) {
			return fahrzeugTyp != null && fahrzeugTyp.getTargetId().equals(productCmpt.getId())
					? fahrzeugTyp.getCardinality()
					: null;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void doInitPropertiesFromXml(Map<String, Element> configMap) {
		super.doInitPropertiesFromXml(configMap);
	}

	/**
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void doInitReferencesFromXml(Map<String, List<Element>> elementsMap) {
		super.doInitReferencesFromXml(elementsMap);
		doInitFahrzeugTyp(elementsMap);
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitFahrzeugTyp(Map<String, List<Element>> elementsMap) {
		List<Element> associationElements = elementsMap.get(XML_TAG_FAHRZEUG_TYP);
		if (associationElements != null) {
			Element element = associationElements.get(0);
			fahrzeugTyp = new ProductComponentLink<>(this);
			fahrzeugTyp.initFromXml(element);
		}
	}

	/**
	 * Creates a new HaftpflichtVersicherung that is configured.
	 *
	 * @generated
	 */
	@IpsGenerated
	public HaftpflichtVersicherung createHaftpflichtVersicherung() {
		HaftpflichtVersicherung policy = new HaftpflichtVersicherung(this);
		policy.initialize();
		return policy;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public HaftpflichtVersicherung createPolicyComponent() {
		return createHaftpflichtVersicherung();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IProductComponentLink<? extends IProductComponent> getLink(String linkName, IProductComponent target) {
		if ("FahrzeugTyp".equals(linkName)) {
			return getLinkForFahrzeugTyp((FahrzeugTyp) target);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public List<IProductComponentLink<? extends IProductComponent>> getLinks() {
		List<IProductComponentLink<? extends IProductComponent>> list = new ArrayList<>();
		if (getLinkForFahrzeugTyp() != null) {
			list.add(getLinkForFahrzeugTyp());
		}
		return list;
	}

	/**
	 * Creates a new instance of HaftpflichtVersicherungsTypBuilder to edit this
	 * product.
	 *
	 * @generated
	 */
	@IpsGenerated
	public HaftpflichtVersicherungsTypBuilder modify() {
		return HaftpflichtVersicherungsTypBuilder.from(this, (InMemoryRuntimeRepository) this.getRepository());
	}

	/**
	 * Generates a new instance of HaftpflichtVersicherungsTyp with a given
	 * {@link InMemoryRuntimeRepository}, ID, kindID and versionID. The new product
	 * is set to be valid from 1900/1/1.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static HaftpflichtVersicherungsTypBuilder builder(InMemoryRuntimeRepository runtimeRepository, String id,
			String kindId, String versionId) {
		HaftpflichtVersicherungsTyp product = new HaftpflichtVersicherungsTyp(runtimeRepository, id, kindId, versionId);
		product.setValidFrom(new DateTime(1900, 1, 1));
		runtimeRepository.putProductComponent(product);

		return new HaftpflichtVersicherungsTypBuilder(product, runtimeRepository);
	}

	/**
	 * Generates a new instance of HaftpflichtVersicherungsTyp with a given
	 * {@link InMemoryRuntimeRepository}, ID, kindID and versionID.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static HaftpflichtVersicherungsTypBuilder builder(InMemoryRuntimeRepository runtimeRepository, String id,
			String kindId, String versionId, DateTime validFrom) {
		HaftpflichtVersicherungsTyp product = new HaftpflichtVersicherungsTyp(runtimeRepository, id, kindId, versionId);
		product.setValidFrom(validFrom);
		runtimeRepository.putProductComponent(product);

		return new HaftpflichtVersicherungsTypBuilder(product, runtimeRepository);
	}

	/**
	 * Generates a new instance of HaftpflichtVersicherungsTyp with the ID of an
	 * existing product component.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static HaftpflichtVersicherungsTypBuilder builder(InMemoryRuntimeRepository runtimeRepository,
			String prodCmptId) {
		HaftpflichtVersicherungsTyp product = (HaftpflichtVersicherungsTyp) runtimeRepository
				.getProductComponent(prodCmptId);

		if (product == null) {
			throw new RuntimeException("No product component found with given ID!");
		} else {
			return HaftpflichtVersicherungsTypBuilder.from(product, runtimeRepository);
		}
	}
}
