package at.faktorzehn.kfz.model;

import org.faktorips.runtime.model.annotation.IpsPolicyCmptType;
import org.faktorips.runtime.model.annotation.IpsAssociations;
import org.faktorips.runtime.model.annotation.IpsConfiguredBy;
import org.faktorips.runtime.model.annotation.IpsDocumented;
import org.faktorips.runtime.internal.AbstractModelObject;
import org.faktorips.runtime.IDeltaSupport;
import org.faktorips.runtime.ICopySupport;
import org.faktorips.runtime.IVisitorSupport;
import org.faktorips.runtime.IDependantObject;
import org.faktorips.runtime.IConfigurableModelObject;
import org.faktorips.valueset.IntegerRange;
import org.faktorips.runtime.internal.ProductConfiguration;
import org.faktorips.runtime.model.annotation.IpsAssociation;
import org.faktorips.runtime.model.type.AssociationKind;
import org.faktorips.runtime.model.annotation.IpsMatchingAssociation;
import org.faktorips.runtime.model.annotation.IpsInverseAssociation;
import org.faktorips.runtime.model.annotation.IpsAssociationAdder;
import org.w3c.dom.Element;
import org.faktorips.runtime.IUnresolvedReference;
import org.faktorips.runtime.DefaultUnresolvedReference;
import org.faktorips.runtime.IModelObjectDelta;
import org.faktorips.runtime.IModelObject;
import org.faktorips.runtime.IProductComponent;
import java.util.Calendar;
import org.faktorips.runtime.IDeltaComputationOptions;
import org.faktorips.runtime.internal.ModelObjectDelta;
import java.util.Map;
import java.util.HashMap;
import org.faktorips.runtime.IModelObjectVisitor;
import org.faktorips.runtime.MessageList;
import org.faktorips.runtime.IValidationContext;
import org.faktorips.runtime.IRuntimeRepository;
import org.faktorips.runtime.IObjectReferenceStore;
import org.faktorips.runtime.internal.XmlCallback;
import org.faktorips.runtime.annotation.IpsGenerated;

/**
 * Implementation for KaskoVersicherung.
 * 
 * @since 1.0.0
 *
 * @generated
 */
@IpsPolicyCmptType(name = "KaskoVersicherung")
@IpsAssociations({ "Fahrzeug", "KfzVertrag" })
@IpsConfiguredBy(KaskoVersicherungsTyp.class)
@IpsDocumented(bundleName = "at.faktorzehn.kfz.model.model-label-and-descriptions", defaultLocale = "de")
public class KaskoVersicherung extends AbstractModelObject
		implements IDeltaSupport, ICopySupport, IVisitorSupport, IDependantObject, IConfigurableModelObject {

	/**
	 * The maximal multiplicity of the association with the role name Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final IntegerRange MAX_MULTIPLICITY_OF_FAHRZEUG = IntegerRange.valueOf(0, 1);
	/**
	 * The name of the association fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String ASSOCIATION_FAHRZEUG = "fahrzeug";
	/**
	 * The name of the association kfzVertrag.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String ASSOCIATION_KFZ_VERTRAG = "kfzVertrag";
	/**
	 * References the current product configuration.
	 *
	 * @generated
	 */
	private ProductConfiguration productConfiguration;
	/**
	 * Member variable for the association with the role name Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private Fahrzeug fahrzeug = null;

	/**
	 * Member variable for the parent object: KfzVertrag.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private KfzVertrag kfzVertrag;

	/**
	 * Creates a new KaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public KaskoVersicherung() {
		super();
		productConfiguration = new ProductConfiguration();
	}

	/**
	 * Creates a new KaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public KaskoVersicherung(KaskoVersicherungsTyp productCmpt) {
		super();
		productConfiguration = new ProductConfiguration(productCmpt);
	}

	/**
	 * Returns the referenced Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "Fahrzeug", pluralName = "Fahrzeuge", kind = AssociationKind.Association, targetClass = Fahrzeug.class, min = 0, max = 1)
	@IpsMatchingAssociation(source = KaskoVersicherungsTyp.class, name = "FahrzeugTyp")
	@IpsInverseAssociation("KaskoVersicherung")
	@IpsGenerated
	public Fahrzeug getFahrzeug() {
		return fahrzeug;
	}

	/**
	 * Sets the Fahrzeug.
	 * 
	 * @throws ClassCastException If the association is constrained and the given
	 *                            object is not of the correct type.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "Fahrzeug")
	@IpsGenerated
	public void setFahrzeug(Fahrzeug newObject) {
		if (newObject == fahrzeug) {
			return;
		}
		Fahrzeug oldRefObject = fahrzeug;
		fahrzeug = null;
		if (oldRefObject != null) {
			oldRefObject.setKaskoVersicherung(null);
		}
		fahrzeug = newObject;
		if (fahrzeug != null && fahrzeug.getKaskoVersicherung() != this) {
			fahrzeug.setKaskoVersicherung(this);
		}
	}

	/**
	 * Returns the referenced KfzVertrag.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "KfzVertrag", pluralName = "KfzVertraege", kind = AssociationKind.CompositionToMaster, targetClass = KfzVertrag.class, min = 0, max = 1)
	@IpsInverseAssociation("KaskoVersicherung")
	@IpsGenerated
	public KfzVertrag getKfzVertrag() {
		return kfzVertrag;
	}

	/**
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "KfzVertrag")
	@IpsGenerated
	public void setKfzVertragInternal(KfzVertrag newParent) {
		if (getKfzVertrag() == newParent) {
			return;
		}
		IModelObject parent = getParentModelObject();
		if (newParent != null && parent != null) {
			throw new IllegalStateException(String.format(
					"KaskoVersicherung (\"%s\") can't be assigned to parent object of class KfzVertrag (\"%s\"), because object already belongs to the parent object (\"%s\").",
					toString(), newParent.toString(), parent.toString()));
		}
		this.kfzVertrag = newParent;
		effectiveFromHasChanged();
	}

	/**
	 * Initializes the object with the configured defaults.
	 *
	 * @restrainedmodifiable
	 */
	@Override
	@IpsGenerated
	public void initialize() {
		// begin-user-code
		// end-user-code
	}

	/**
	 * Returns the KaskoVersicherungsTyp that configures this object.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KaskoVersicherungsTyp getKaskoVersicherungsTyp() {
		return (KaskoVersicherungsTyp) getProductComponent();
	}

	/**
	 * Sets the new KaskoVersicherungsTyp that configures this object.
	 * 
	 * @param kaskoVersicherungsTyp                  The new KaskoVersicherungsTyp.
	 * @param initPropertiesWithConfiguratedDefaults <code>true</code> if the
	 *                                               properties should be
	 *                                               initialized with the defaults
	 *                                               defined in the
	 *                                               KaskoVersicherungsTyp.
	 *
	 * @generated
	 */
	@IpsGenerated
	public void setKaskoVersicherungsTyp(KaskoVersicherungsTyp kaskoVersicherungsTyp,
			boolean initPropertiesWithConfiguratedDefaults) {
		setProductComponent(kaskoVersicherungsTyp);
		if (initPropertiesWithConfiguratedDefaults) {
			initialize();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IProductComponent getProductComponent() {
		return productConfiguration.getProductComponent();
	}

	/**
	 * Sets the current product component.
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public void setProductComponent(IProductComponent productComponent) {
		productConfiguration.setProductComponent(productComponent);
	}

	/**
	 * This method is called when the effective from date has changed, so that the
	 * reference to the product component generation can be cleared. If this policy
	 * component contains child components, this method will also clear the
	 * reference to their product component generations.
	 * <p>
	 * The product component generation is cleared if and only if there is a new
	 * effective from date. If {@link #getEffectiveFromAsCalendar()} returns
	 * <code>null</code> the product component generation is not reset, for example
	 * if this model object was removed from its parent.
	 * <p>
	 * Clients may change the behavior of resetting the product component by
	 * overwriting {@link #resetProductCmptGenerationAfterEffectiveFromHasChanged()}
	 * instead of this method.
	 *
	 * @generated
	 */
	@IpsGenerated
	public void effectiveFromHasChanged() {
		if (getEffectiveFromAsCalendar() != null) {
			resetProductCmptGenerationAfterEffectiveFromHasChanged();
		}
	}

	/**
	 * Clears the product component generation.
	 * <p>
	 * This method can be overwritten to affect the behavior in case of an
	 * effective-date change.
	 *
	 * @generated
	 */
	@IpsGenerated
	protected void resetProductCmptGenerationAfterEffectiveFromHasChanged() {
		productConfiguration.resetProductCmptGeneration();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public Calendar getEffectiveFromAsCalendar() {
		IModelObject parent = getParentModelObject();
		if (parent instanceof IConfigurableModelObject) {
			return ((IConfigurableModelObject) parent).getEffectiveFromAsCalendar();
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IModelObject getParentModelObject() {
		if (kfzVertrag != null) {
			return kfzVertrag;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void initFromXml(Element objectEl, boolean initWithProductDefaultsBeforeReadingXmlData,
			IRuntimeRepository productRepository, IObjectReferenceStore store, XmlCallback xmlCallback,
			String currPath) {
		productConfiguration.initFromXml(objectEl, productRepository);
		if (initWithProductDefaultsBeforeReadingXmlData) {
			initialize();
		}
		super.initFromXml(objectEl, initWithProductDefaultsBeforeReadingXmlData, productRepository, store, xmlCallback,
				currPath);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected AbstractModelObject createChildFromXml(Element childEl) {
		AbstractModelObject newChild = super.createChildFromXml(childEl);
		if (newChild != null) {
			return newChild;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected IUnresolvedReference createUnresolvedReference(Object objectId, String targetRole, String targetId)
			throws Exception {
		if ("Fahrzeug".equals(targetRole)) {
			return new DefaultUnresolvedReference(this, objectId, "setFahrzeug", Fahrzeug.class, targetId);
		}
		return super.createUnresolvedReference(objectId, targetRole, targetId);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IModelObjectDelta computeDelta(IModelObject otherObject, IDeltaComputationOptions options) {
		ModelObjectDelta delta = ModelObjectDelta.newDelta(this, otherObject, options);
		if (!KaskoVersicherung.class.isAssignableFrom(otherObject.getClass())) {
			return delta;
		}
		KaskoVersicherung otherKaskoVersicherung = (KaskoVersicherung) otherObject;
		if (!options.ignoreAssociations()) {
			ModelObjectDelta.createAssociatedChildDeltas(delta, fahrzeug, otherKaskoVersicherung.fahrzeug,
					ASSOCIATION_FAHRZEUG, options);
		}
		return delta;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public KaskoVersicherung newCopy() {
		Map<IModelObject, IModelObject> copyMap = new HashMap<>();
		KaskoVersicherung newCopy = newCopyInternal(copyMap);
		copyAssociationsInternal(newCopy, copyMap);
		return newCopy;
	}

	/**
	 * Internal copy method with a {@link Map} containing already copied instances.
	 * 
	 * @param copyMap the map contains the copied instances
	 *
	 * @generated
	 */
	@IpsGenerated
	public KaskoVersicherung newCopyInternal(Map<IModelObject, IModelObject> copyMap) {
		KaskoVersicherung newCopy = (KaskoVersicherung) copyMap.get(this);
		if (newCopy == null) {
			newCopy = new KaskoVersicherung();
			copyMap.put(this, newCopy);
			newCopy.copyProductCmptAndGenerationInternal(this);
			copyProperties(newCopy, copyMap);
		}
		return newCopy;
	}

	/**
	 * Copies the product component and product component generation from the other
	 * object.
	 *
	 * @generated
	 */
	@IpsGenerated
	protected void copyProductCmptAndGenerationInternal(KaskoVersicherung otherObject) {
		productConfiguration.copy(otherObject.productConfiguration);
	}

	/**
	 * This method sets all properties in the copy with the values of this object.
	 * If there are copied associated objects they are added to the copyMap in
	 * {@link #newCopyInternal(Map)}.
	 * 
	 * @param copy    The copy object
	 * @param copyMap a map containing copied associated objects
	 *
	 * @generated
	 */
	@IpsGenerated
	protected void copyProperties(IModelObject copy, Map<IModelObject, IModelObject> copyMap) {
		KaskoVersicherung concreteCopy = (KaskoVersicherung) copy;
		concreteCopy.fahrzeug = fahrzeug;
	}

	/**
	 * Internal method for setting copied associations. For copied targets, the
	 * associations have to be retargeted to the new copied instances. This method
	 * have to call {@link #copyAssociationsInternal(IModelObject, Map)} in other
	 * instances associated by composite.
	 * 
	 * @param abstractCopy the copy of this policy component
	 * @param copyMap      the map contains the copied instances
	 *
	 * @generated
	 */
	@IpsGenerated
	public void copyAssociationsInternal(IModelObject abstractCopy, Map<IModelObject, IModelObject> copyMap) {
		KaskoVersicherung newCopy = (KaskoVersicherung) abstractCopy;
		if (copyMap.containsKey(fahrzeug)) {
			newCopy.fahrzeug = (Fahrzeug) copyMap.get(fahrzeug);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public boolean accept(IModelObjectVisitor visitor) {
		if (!visitor.visit(this)) {
			return false;
		}
		return true;
	}

	/**
	 * Validates the object (but not its children). Returns <code>true</code> if
	 * this object should continue validating, <code>false</code> otherwise.
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public boolean validateSelf(MessageList ml, IValidationContext context) {
		if (!super.validateSelf(ml, context)) {
			return STOP_VALIDATION;
		}
		return CONTINUE_VALIDATION;
	}

	/**
	 * Validates the object's children.
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public void validateDependants(MessageList ml, IValidationContext context) {
		super.validateDependants(ml, context);
	}

	/**
	 * @restrainedmodifiable
	 */
	@Override
	@IpsGenerated
	public String toString() {
		// begin-user-code
		return getProductComponent() == null ? getClass().getSimpleName()
				: getClass().getSimpleName() + '[' + getProductComponent().toString() + ']';
		// end-user-code
	}

	/**
	 * Creates a new instance of KaskoVersicherungBuilder to edit this policy.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KaskoVersicherungBuilder modify() {
		return KaskoVersicherungBuilder.from(this, getProductComponent().getRepository());
	}

	/**
	 * The runtime repository is used to create configured association targets from
	 * existing product components.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KaskoVersicherungBuilder modify(IRuntimeRepository runtimeRepository) {
		return KaskoVersicherungBuilder.from(this, runtimeRepository);
	}

	/**
	 * Creates a new KaskoVersicherungBuilder with a new instance of policy. Runtime
	 * repository is set to null.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KaskoVersicherungBuilder builder() {
		return KaskoVersicherungBuilder.from(new KaskoVersicherung(), null);
	}

	/**
	 * Creates a new KaskoVersicherungBuilder with a new instance of policy. Runtime
	 * repository is set to null. The runtime repository is required as association
	 * targets exists that are configured by a product. The product components of
	 * the targets must be in the given runtime repository.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KaskoVersicherungBuilder builder(IRuntimeRepository runtimeRepository) {
		return KaskoVersicherungBuilder.from(new KaskoVersicherung(), runtimeRepository);
	}

	/**
	 * Creates a new KaskoVersicherungBuilder with a new instance of policy created
	 * by the given product component.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KaskoVersicherungBuilder builder(KaskoVersicherungsTyp productCmpt) {
		return KaskoVersicherungBuilder.from(new KaskoVersicherung(productCmpt), productCmpt.getRepository());
	}

	/**
	 * Creates a new KaskoVersicherungBuilder with a new instance of policy created
	 * by the product component with the given ID in the runtime repository that
	 * configures the policy.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KaskoVersicherungBuilder builder(IRuntimeRepository runtimeRepository, String productCmptId) {
		KaskoVersicherungsTyp product = (KaskoVersicherungsTyp) runtimeRepository.getProductComponent(productCmptId);
		if (product == null) {
			throw new RuntimeException("No product component found with given ID!");
		} else {
			KaskoVersicherung policy = product.createKaskoVersicherung();

			policy.initialize();
			return KaskoVersicherungBuilder.from(policy, runtimeRepository);
		}
	}

}
