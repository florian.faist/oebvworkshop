package at.faktorzehn.kfz.model;

import org.faktorips.runtime.InMemoryRuntimeRepository;
import org.faktorips.runtime.CardinalityRange;
import org.faktorips.runtime.annotation.IpsGenerated;

/**
 * Implementation of KaskoVersicherungsTypBuilder. A
 * KaskoVersicherungsTypBuilder provides all functionalities that is needed to
 * create a KaskoVersicherungsTyp. Note that this is only for testing purpose.
 * All instances of product have to be created in a new
 * {@link InMemoryRuntimeRepository} as Product Component in the runtime
 * repository can not be edited.
 *
 * @generated
 */
public class KaskoVersicherungsTypBuilder {
	/**
	 * @generated
	 */
	private final InMemoryRuntimeRepository runtimeRepository;

	/**
	 * @generated
	 */
	private final KaskoVersicherungsTyp kaskoVersicherungsTyp;

	/**
	 * Generates a new instance of KaskoVersicherungsTypBuilder with a given
	 * product. The product must exist in the given
	 * {@link InMemoryRuntimeRepository} that is not allowed to be null.
	 *
	 * @generated
	 */
	@IpsGenerated
	protected KaskoVersicherungsTypBuilder(KaskoVersicherungsTyp product, InMemoryRuntimeRepository runtimeRepository) {
		if (product == null || runtimeRepository == null) {
			throw new RuntimeException("Product and repository can not be null!");
		} else {
			runtimeRepository.getExistingProductComponent(product.getId());

			this.runtimeRepository = runtimeRepository;
			this.kaskoVersicherungsTyp = product;
		}
	}

	/**
	 * @return {@link InMemoryRuntimeRepository} that is saved.
	 *
	 * @generated
	 */
	@IpsGenerated
	public InMemoryRuntimeRepository getRepository() {
		return this.runtimeRepository;
	}

	/**
	 * @return instance of KaskoVersicherungsTyp that is built.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KaskoVersicherungsTyp getResult() {
		return kaskoVersicherungsTyp;
	}

	/**
	 * Interne Methode
	 *
	 * @generated
	 */
	@IpsGenerated
	public static Class<?> getProductClass() {
		return KaskoVersicherungsTyp.class;
	}

	/**
	 * Creates a new KaskoVersicherungsTypBuilder with a given KaskoVersicherungsTyp
	 * and a runtime repository.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KaskoVersicherungsTypBuilder from(KaskoVersicherungsTyp product,
			InMemoryRuntimeRepository runtimeRepository) {
		return new KaskoVersicherungsTypBuilder(product, runtimeRepository);
	}

	/**
	 * Returns an {@link AssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AssociationBuilder} you
	 * get the builder of the target object for further processing. Use the method
	 * {@link #add()} if you want to create multiple target objects and always
	 * return to this builder instead of the target builder.
	 * 
	 * @see #add()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AssociationBuilder associate() {
		return new AssociationBuilder(this);
	}

	/**
	 * Returns an {@link AddAssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AddAssociationBuilder}
	 * you always could return to this builder for further processing. Use the
	 * method {@link #associate()} if you want to have the builder of the target
	 * object.
	 * 
	 * @see #associate()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AddAssociationBuilder add() {
		return new AddAssociationBuilder(this);
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * returns a builder for the target class.
	 *
	 * @generated
	 */
	public static class AssociationBuilder {

		/**
		 * @generated
		 */
		private KaskoVersicherungsTypBuilder productBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AssociationBuilder(KaskoVersicherungsTypBuilder productBuilder) {
			this.productBuilder = productBuilder;
		}

		/**
		 * Gets a instance of a subclass of FahrzeugTyp with the ID of the product
		 * component and sets it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends FahrzeugTypBuilder> T fahrzeugTyp(T targetBuilder) {
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Gets a instance of a subclass of FahrzeugTyp with the ID of the product
		 * component and sets it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends FahrzeugTypBuilder> T fahrzeugTyp(T targetBuilder, CardinalityRange cardinality) {
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * Gets a instance of FahrzeugTyp with the ID of the product component and sets
		 * it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder fahrzeugTyp(String productCmptId) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), productCmptId);
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Generates a new Instance of FahrzeugTyp with the IDs and sets it as
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder fahrzeugTyp(String id, String kindId, String versionId) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), id, kindId, versionId);
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Gets a instance of FahrzeugTyp with the ID of the product component and sets
		 * it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder fahrzeugTyp(String productCmptId, CardinalityRange cardinality) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), productCmptId);
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * Generates a new Instance of FahrzeugTyp with the IDs and sets it as
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder fahrzeugTyp(String id, String kindId, String versionId,
				CardinalityRange cardinality) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), id, kindId, versionId);
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected KaskoVersicherungsTyp getResult() {
			return productBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected InMemoryRuntimeRepository getRepository() {
			return productBuilder.getRepository();
		}
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * return the original KaskoVersicherungsTypBuilder.
	 *
	 * @generated
	 */
	public static class AddAssociationBuilder {

		/**
		 * @generated
		 */
		private KaskoVersicherungsTypBuilder productBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AddAssociationBuilder(KaskoVersicherungsTypBuilder productBuilder) {
			this.productBuilder = productBuilder;
		}

		/**
		 * Sets a existing instance of FahrzeugTyp as the target of the association
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder fahrzeugTyp(FahrzeugTyp targetProduct) {
			getResult().setFahrzeugTyp(targetProduct);
			return done();
		}

		/**
		 * Sets a existing instance of FahrzeugTyp as the target of the association
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder fahrzeugTyp(FahrzeugTyp targetProduct, CardinalityRange cardinality) {
			getResult().setFahrzeugTyp(targetProduct, cardinality);
			return done();
		}

		/**
		 * Gets a instance of a subclass of FahrzeugTyp with the ID of the product
		 * component and sets it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder fahrzeugTyp(FahrzeugTypBuilder targetBuilder) {
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Gets a instance of a subclass of FahrzeugTyp with the ID of the product
		 * component and sets it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder fahrzeugTyp(FahrzeugTypBuilder targetBuilder,
				CardinalityRange cardinality) {
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * Gets a instance of FahrzeugTyp with the ID of the product component and sets
		 * it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder fahrzeugTyp(String productCmptId) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), productCmptId);
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Generates a new Instance of FahrzeugTyp with the IDs and sets it as
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder fahrzeugTyp(String id, String kindId, String versionId) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), id, kindId, versionId);
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Gets a instance of FahrzeugTyp with the ID of the product component and sets
		 * it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder fahrzeugTyp(String productCmptId, CardinalityRange cardinality) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), productCmptId);
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * Generates a new Instance of FahrzeugTyp with the IDs and sets it as
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder fahrzeugTyp(String id, String kindId, String versionId,
				CardinalityRange cardinality) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), id, kindId, versionId);
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected KaskoVersicherungsTypBuilder done() {
			return productBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected KaskoVersicherungsTyp getResult() {
			return productBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected InMemoryRuntimeRepository getRepository() {
			return productBuilder.getRepository();
		}
	}

}
