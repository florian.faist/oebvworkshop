package at.faktorzehn.kfz.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.faktorips.runtime.CardinalityRange;
import org.faktorips.runtime.IProductComponent;
import org.faktorips.runtime.IProductComponentLink;
import org.faktorips.runtime.IRuntimeRepository;
import org.faktorips.runtime.IValidationContext;
import org.faktorips.runtime.IllegalRepositoryModificationException;
import org.faktorips.runtime.InMemoryRuntimeRepository;
import org.faktorips.runtime.annotation.IpsGenerated;
import org.faktorips.runtime.internal.DateTime;
import org.faktorips.runtime.internal.EnumValues;
import org.faktorips.runtime.internal.IpsStringUtils;
import org.faktorips.runtime.internal.MultiValueXmlHelper;
import org.faktorips.runtime.internal.ProductComponent;
import org.faktorips.runtime.internal.ProductComponentLink;
import org.faktorips.runtime.internal.ValueToXmlHelper;
import org.faktorips.runtime.model.annotation.IpsAllowedValues;
import org.faktorips.runtime.model.annotation.IpsAllowedValuesSetter;
import org.faktorips.runtime.model.annotation.IpsAssociation;
import org.faktorips.runtime.model.annotation.IpsAssociationAdder;
import org.faktorips.runtime.model.annotation.IpsAssociationLinks;
import org.faktorips.runtime.model.annotation.IpsAssociations;
import org.faktorips.runtime.model.annotation.IpsAttribute;
import org.faktorips.runtime.model.annotation.IpsAttributeSetter;
import org.faktorips.runtime.model.annotation.IpsAttributes;
import org.faktorips.runtime.model.annotation.IpsConfigures;
import org.faktorips.runtime.model.annotation.IpsDefaultValue;
import org.faktorips.runtime.model.annotation.IpsDefaultValueSetter;
import org.faktorips.runtime.model.annotation.IpsDocumented;
import org.faktorips.runtime.model.annotation.IpsMatchingAssociation;
import org.faktorips.runtime.model.annotation.IpsProductCmptType;
import org.faktorips.runtime.model.type.AssociationKind;
import org.faktorips.runtime.model.type.AttributeKind;
import org.faktorips.runtime.model.type.ValueSetKind;
import org.faktorips.values.ListUtil;
import org.faktorips.valueset.OrderedValueSet;
import org.faktorips.valueset.ValueSet;
import org.w3c.dom.Element;

/**
 * Implementation for KfzProdukt.
 * 
 * @since 1.0.0
 *
 * @generated
 */
@IpsProductCmptType(name = "KfzProdukt")
@IpsAttributes({ "name", "psBerechnungsFaktor", "erlaubteMarken" })
@IpsAssociations({ "FahrzeugTyp", "HaftpflichtVersicherungsTyp", "KaskoVersicherungsTyp" })
@IpsConfigures(KfzVertrag.class)
@IpsDocumented(bundleName = "at.faktorzehn.kfz.model.model-label-and-descriptions", defaultLocale = "de")
public class KfzProdukt extends ProductComponent {

	/**
	 * The name of the XML tag for the association fahrzeugTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private static final String XML_TAG_FAHRZEUG_TYP = "FahrzeugTyp";
	/**
	 * The name of the XML tag for the association haftpflichtVersicherungsTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private static final String XML_TAG_HAFTPFLICHT_VERSICHERUNGS_TYP = "HaftpflichtVersicherungsTyp";
	/**
	 * The name of the XML tag for the association kaskoVersicherungsTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private static final String XML_TAG_KASKO_VERSICHERUNGS_TYP = "KaskoVersicherungsTyp";
	/**
	 * The name of the property name.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_NAME = "name";
	/**
	 * The name of the property psBerechnungsFaktor.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_PSBERECHNUNGSFAKTOR = "psBerechnungsFaktor";
	/**
	 * The name of the property erlaubteMarken.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_ERLAUBTEMARKEN = "erlaubteMarken";
	/**
	 * The product component property Name.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private String name;
	/**
	 * The product component property PsBerechnungsFaktor.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private Double psBerechnungsFaktor;
	/**
	 * The product component property ErlaubteMarken.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private List<FahrzeugMarken> erlaubteMarken;
	/**
	 * The default value for zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private Integer defaultValueZahlweise = null;
	/**
	 * Allowed set of values for property zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private OrderedValueSet<Integer> allowedValuesForZahlweise = KfzVertrag.MAX_ALLOWED_VALUES_FOR_ZAHLWEISE;

	/**
	 * Member variable for the association FahrzeugTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private IProductComponentLink<FahrzeugTyp> fahrzeugTyp = null;

	/**
	 * Member variable for the association HaftpflichtVersicherungsTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private IProductComponentLink<HaftpflichtVersicherungsTyp> haftpflichtVersicherungsTyp = null;

	/**
	 * Member variable for the association KaskoVersicherungsTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private IProductComponentLink<KaskoVersicherungsTyp> kaskoVersicherungsTyp = null;

	/**
	 * Creates a new KfzProdukt.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzProdukt(IRuntimeRepository repository, String id, String kindId, String versionId) {
		super(repository, id, kindId, versionId);
		setErlaubteMarkenInternal(ListUtil.newList(FahrzeugMarken.BMW));
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public boolean isChangingOverTime() {
		return false;
	}

	/**
	 * Returns the value of name.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttribute(name = "name", kind = AttributeKind.CONSTANT, valueSetKind = ValueSetKind.AllValues)
	@IpsGenerated
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of attribute name.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttributeSetter("name")
	@IpsGenerated
	public void setName(String newValue) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		setNameInternal(newValue);
	}

	/**
	 * Sets the value of attribute name.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	protected final void setNameInternal(String newValue) {
		this.name = newValue;
	}

	/**
	 * Returns the value of psBerechnungsFaktor.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttribute(name = "psBerechnungsFaktor", kind = AttributeKind.CONSTANT, valueSetKind = ValueSetKind.AllValues)
	@IpsGenerated
	public Double getPsBerechnungsFaktor() {
		return psBerechnungsFaktor;
	}

	/**
	 * Sets the value of attribute psBerechnungsFaktor.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttributeSetter("psBerechnungsFaktor")
	@IpsGenerated
	public void setPsBerechnungsFaktor(Double newValue) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		setPsBerechnungsFaktorInternal(newValue);
	}

	/**
	 * Sets the value of attribute psBerechnungsFaktor.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	protected final void setPsBerechnungsFaktorInternal(Double newValue) {
		this.psBerechnungsFaktor = newValue;
	}

	/**
	 * Returns the value of erlaubteMarken.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttribute(name = "erlaubteMarken", kind = AttributeKind.CONSTANT, valueSetKind = ValueSetKind.AllValues)
	@IpsGenerated
	public List<FahrzeugMarken> getErlaubteMarken() {
		return new ArrayList<>(erlaubteMarken);
	}

	/**
	 * Sets the value of attribute erlaubteMarken.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttributeSetter("erlaubteMarken")
	@IpsGenerated
	public void setErlaubteMarken(List<FahrzeugMarken> newValue) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		setErlaubteMarkenInternal(new ArrayList<>(newValue));
	}

	/**
	 * Sets the value of attribute erlaubteMarken.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	protected final void setErlaubteMarkenInternal(List<FahrzeugMarken> newValue) {
		this.erlaubteMarken = newValue;
	}

	/**
	 * Returns the default value for zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsDefaultValue("zahlweise")
	@IpsGenerated
	public Integer getDefaultValueZahlweise() {
		return defaultValueZahlweise;
	}

	/**
	 * Sets the default value for zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsDefaultValueSetter("zahlweise")
	@IpsGenerated
	public void setDefaultValueZahlweise(Integer defaultValueZahlweise) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		this.defaultValueZahlweise = defaultValueZahlweise;
	}

	/**
	 * Returns the set of allowed values for the property zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAllowedValues("zahlweise")
	@IpsGenerated
	public OrderedValueSet<Integer> getAllowedValuesForZahlweise(IValidationContext context) {
		return allowedValuesForZahlweise;
	}

	/**
	 * Sets the set of allowed values for the property zahlweise.
	 * 
	 * @throws ClassCastException if the type of value set does not match the
	 *                            property's configuration.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAllowedValuesSetter("zahlweise")
	@IpsGenerated
	public void setAllowedValuesForZahlweise(ValueSet<Integer> allowedValuesForZahlweise) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		this.allowedValuesForZahlweise = (OrderedValueSet<Integer>) allowedValuesForZahlweise;
	}

	/**
	 * Returns the referenced FahrzeugTyp or <code>null</code> if there is no
	 * referenced FahrzeugTyp.
	 * 
	 * @throws org.faktorips.runtime.ProductCmptNotFoundException if a product
	 *                                                            component is
	 *                                                            referenced but can
	 *                                                            not be found.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "FahrzeugTyp", pluralName = "FahrzeugTypen", kind = AssociationKind.Composition, targetClass = FahrzeugTyp.class, min = 1, max = 1)
	@IpsMatchingAssociation(source = KfzVertrag.class, name = "Fahrzeug")
	@IpsGenerated
	public FahrzeugTyp getFahrzeugTyp() {
		return fahrzeugTyp != null ? fahrzeugTyp.getTarget() : null;
	}

	/**
	 * Sets the new FahrzeugTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "FahrzeugTyp")
	@IpsGenerated
	public void setFahrzeugTyp(FahrzeugTyp target) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		fahrzeugTyp = (target == null ? null : new ProductComponentLink<>(this, target, "FahrzeugTyp"));
	}

	/**
	 * Sets the new FahrzeugTyp with the given cardinality.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "FahrzeugTyp", withCardinality = true)
	@IpsGenerated
	public void setFahrzeugTyp(FahrzeugTyp target, CardinalityRange cardinality) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		fahrzeugTyp = (target == null ? null : new ProductComponentLink<>(this, target, cardinality, "FahrzeugTyp"));
	}

	/**
	 * Returns the <code>ILink</code> to the FahrzeugTyp or <code>null</code>, if no
	 * object is referenced.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationLinks(association = "FahrzeugTyp")
	@IpsGenerated
	public IProductComponentLink<FahrzeugTyp> getLinkForFahrzeugTyp() {
		return fahrzeugTyp;
	}

	/**
	 * Returns the <code>ILink</code> to the FahrzeugTyp at the indicated index.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public IProductComponentLink<FahrzeugTyp> getLinkForFahrzeugTyp(FahrzeugTyp productComponent) {
		return fahrzeugTyp != null && fahrzeugTyp.getTargetId().equals(productComponent.getId()) ? fahrzeugTyp : null;
	}

	/**
	 * Returns the cardinality for the number of allowed instanced for Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public CardinalityRange getCardinalityForFahrzeug(FahrzeugTyp productCmpt) {
		if (productCmpt != null) {
			return fahrzeugTyp != null && fahrzeugTyp.getTargetId().equals(productCmpt.getId())
					? fahrzeugTyp.getCardinality()
					: null;
		}
		return null;
	}

	/**
	 * Returns the referenced HaftpflichtVersicherungsTyp or <code>null</code> if
	 * there is no referenced HaftpflichtVersicherungsTyp.
	 * 
	 * @throws org.faktorips.runtime.ProductCmptNotFoundException if a product
	 *                                                            component is
	 *                                                            referenced but can
	 *                                                            not be found.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "HaftpflichtVersicherungsTyp", pluralName = "HaftpflichtVersicherungsTypen", kind = AssociationKind.Composition, targetClass = HaftpflichtVersicherungsTyp.class, min = 0, max = 1)
	@IpsMatchingAssociation(source = KfzVertrag.class, name = "HaftpflichtVersicherung")
	@IpsGenerated
	public HaftpflichtVersicherungsTyp getHaftpflichtVersicherungsTyp() {
		return haftpflichtVersicherungsTyp != null ? haftpflichtVersicherungsTyp.getTarget() : null;
	}

	/**
	 * Sets the new HaftpflichtVersicherungsTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "HaftpflichtVersicherungsTyp")
	@IpsGenerated
	public void setHaftpflichtVersicherungsTyp(HaftpflichtVersicherungsTyp target) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		haftpflichtVersicherungsTyp = (target == null ? null
				: new ProductComponentLink<>(this, target, "HaftpflichtVersicherungsTyp"));
	}

	/**
	 * Sets the new HaftpflichtVersicherungsTyp with the given cardinality.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "HaftpflichtVersicherungsTyp", withCardinality = true)
	@IpsGenerated
	public void setHaftpflichtVersicherungsTyp(HaftpflichtVersicherungsTyp target, CardinalityRange cardinality) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		haftpflichtVersicherungsTyp = (target == null ? null
				: new ProductComponentLink<>(this, target, cardinality, "HaftpflichtVersicherungsTyp"));
	}

	/**
	 * Returns the <code>ILink</code> to the HaftpflichtVersicherungsTyp or
	 * <code>null</code>, if no object is referenced.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationLinks(association = "HaftpflichtVersicherungsTyp")
	@IpsGenerated
	public IProductComponentLink<HaftpflichtVersicherungsTyp> getLinkForHaftpflichtVersicherungsTyp() {
		return haftpflichtVersicherungsTyp;
	}

	/**
	 * Returns the <code>ILink</code> to the HaftpflichtVersicherungsTyp at the
	 * indicated index.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public IProductComponentLink<HaftpflichtVersicherungsTyp> getLinkForHaftpflichtVersicherungsTyp(
			HaftpflichtVersicherungsTyp productComponent) {
		return haftpflichtVersicherungsTyp != null
				&& haftpflichtVersicherungsTyp.getTargetId().equals(productComponent.getId())
						? haftpflichtVersicherungsTyp
						: null;
	}

	/**
	 * Returns the cardinality for the number of allowed instanced for
	 * HaftpflichtVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public CardinalityRange getCardinalityForHaftpflichtVersicherung(HaftpflichtVersicherungsTyp productCmpt) {
		if (productCmpt != null) {
			return haftpflichtVersicherungsTyp != null
					&& haftpflichtVersicherungsTyp.getTargetId().equals(productCmpt.getId())
							? haftpflichtVersicherungsTyp.getCardinality()
							: null;
		}
		return null;
	}

	/**
	 * Returns the referenced KaskoVersicherungsTyp or <code>null</code> if there is
	 * no referenced KaskoVersicherungsTyp.
	 * 
	 * @throws org.faktorips.runtime.ProductCmptNotFoundException if a product
	 *                                                            component is
	 *                                                            referenced but can
	 *                                                            not be found.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "KaskoVersicherungsTyp", pluralName = "KaskoVersicherungsTypen", kind = AssociationKind.Composition, targetClass = KaskoVersicherungsTyp.class, min = 0, max = 1)
	@IpsMatchingAssociation(source = KfzVertrag.class, name = "KaskoVersicherung")
	@IpsGenerated
	public KaskoVersicherungsTyp getKaskoVersicherungsTyp() {
		return kaskoVersicherungsTyp != null ? kaskoVersicherungsTyp.getTarget() : null;
	}

	/**
	 * Sets the new KaskoVersicherungsTyp.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "KaskoVersicherungsTyp")
	@IpsGenerated
	public void setKaskoVersicherungsTyp(KaskoVersicherungsTyp target) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		kaskoVersicherungsTyp = (target == null ? null
				: new ProductComponentLink<>(this, target, "KaskoVersicherungsTyp"));
	}

	/**
	 * Sets the new KaskoVersicherungsTyp with the given cardinality.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "KaskoVersicherungsTyp", withCardinality = true)
	@IpsGenerated
	public void setKaskoVersicherungsTyp(KaskoVersicherungsTyp target, CardinalityRange cardinality) {
		if (getRepository() != null && !getRepository().isModifiable()) {
			throw new IllegalRepositoryModificationException();
		}
		kaskoVersicherungsTyp = (target == null ? null
				: new ProductComponentLink<>(this, target, cardinality, "KaskoVersicherungsTyp"));
	}

	/**
	 * Returns the <code>ILink</code> to the KaskoVersicherungsTyp or
	 * <code>null</code>, if no object is referenced.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationLinks(association = "KaskoVersicherungsTyp")
	@IpsGenerated
	public IProductComponentLink<KaskoVersicherungsTyp> getLinkForKaskoVersicherungsTyp() {
		return kaskoVersicherungsTyp;
	}

	/**
	 * Returns the <code>ILink</code> to the KaskoVersicherungsTyp at the indicated
	 * index.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public IProductComponentLink<KaskoVersicherungsTyp> getLinkForKaskoVersicherungsTyp(
			KaskoVersicherungsTyp productComponent) {
		return kaskoVersicherungsTyp != null && kaskoVersicherungsTyp.getTargetId().equals(productComponent.getId())
				? kaskoVersicherungsTyp
				: null;
	}

	/**
	 * Returns the cardinality for the number of allowed instanced for
	 * KaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public CardinalityRange getCardinalityForKaskoVersicherung(KaskoVersicherungsTyp productCmpt) {
		if (productCmpt != null) {
			return kaskoVersicherungsTyp != null && kaskoVersicherungsTyp.getTargetId().equals(productCmpt.getId())
					? kaskoVersicherungsTyp.getCardinality()
					: null;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @since 1.0.0
	 *
	 * @generated NOT
	 */
	public BigDecimal berechneBeitrag(KfzVertrag kfzVertrag) {
		return Optional.ofNullable(kfzVertrag.getFahrzeug()).stream() //
				.max(Comparator.comparing(Fahrzeug::getPS, Comparator.nullsFirst(Comparator.naturalOrder())))//
				.map(Fahrzeug::getPS) //
				.map(ps -> BigDecimal.valueOf(ps).multiply(BigDecimal.valueOf(getPsBerechnungsFaktor())))//
				.orElse(BigDecimal.ZERO);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void doInitPropertiesFromXml(Map<String, Element> configMap) {
		super.doInitPropertiesFromXml(configMap);
		doInitName(configMap);
		doInitPsBerechnungsFaktor(configMap);
		doInitErlaubteMarken(configMap);
		doInitZahlweise(configMap);
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitName(Map<String, Element> configMap) {
		Element configElement = configMap.get(PROPERTY_NAME);
		if (configElement != null) {
			String value = ValueToXmlHelper.getValueFromElement(configElement, ValueToXmlHelper.XML_TAG_VALUE);
			this.name = value;
		}
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitPsBerechnungsFaktor(Map<String, Element> configMap) {
		Element configElement = configMap.get(PROPERTY_PSBERECHNUNGSFAKTOR);
		if (configElement != null) {
			String value = ValueToXmlHelper.getValueFromElement(configElement, ValueToXmlHelper.XML_TAG_VALUE);
			this.psBerechnungsFaktor = IpsStringUtils.isEmpty(value) ? null : Double.valueOf(value);
		}
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitErlaubteMarken(Map<String, Element> configMap) {
		Element configElement = configMap.get(PROPERTY_ERLAUBTEMARKEN);
		if (configElement != null) {
			List<FahrzeugMarken> valueList = new ArrayList<>();
			List<String> stringList = MultiValueXmlHelper.getValuesFromXML(configElement);
			for (String stringValue : stringList) {
				FahrzeugMarken convertedValue = IpsStringUtils.isEmpty(stringValue) ? null
						: FahrzeugMarken.getValueById(stringValue);
				valueList.add(convertedValue);
			}
			this.erlaubteMarken = valueList;
		}
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitZahlweise(Map<String, Element> configMap) {
		Element defaultValueElement = configMap
				.get(ValueToXmlHelper.CONFIGURED_DEFAULT_PREFIX + KfzVertrag.PROPERTY_ZAHLWEISE);
		if (defaultValueElement != null) {
			String value = ValueToXmlHelper.getValueFromElement(defaultValueElement);
			defaultValueZahlweise = IpsStringUtils.isEmpty(value) ? null : Integer.valueOf(value);
		}
		Element valueSetElement = configMap
				.get(ValueToXmlHelper.CONFIGURED_VALUE_SET_PREFIX + KfzVertrag.PROPERTY_ZAHLWEISE);
		if (valueSetElement != null) {
			EnumValues values = ValueToXmlHelper.getEnumValueSetFromElement(valueSetElement,
					ValueToXmlHelper.XML_TAG_VALUE_SET);
			if (values != null) {
				List<Integer> enumValues = new ArrayList<>();
				for (int i = 0; i < values.getNumberOfValues(); i++) {
					enumValues.add(
							IpsStringUtils.isEmpty(values.getValue(i)) ? null : Integer.valueOf(values.getValue(i)));
				}
				allowedValuesForZahlweise = new OrderedValueSet<>(enumValues, values.containsNull(), null);
			}
		}
	}

	/**
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void doInitReferencesFromXml(Map<String, List<Element>> elementsMap) {
		super.doInitReferencesFromXml(elementsMap);
		doInitFahrzeugTyp(elementsMap);
		doInitHaftpflichtVersicherungsTyp(elementsMap);
		doInitKaskoVersicherungsTyp(elementsMap);
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitFahrzeugTyp(Map<String, List<Element>> elementsMap) {
		List<Element> associationElements = elementsMap.get(XML_TAG_FAHRZEUG_TYP);
		if (associationElements != null) {
			Element element = associationElements.get(0);
			fahrzeugTyp = new ProductComponentLink<>(this);
			fahrzeugTyp.initFromXml(element);
		}
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitHaftpflichtVersicherungsTyp(Map<String, List<Element>> elementsMap) {
		List<Element> associationElements = elementsMap.get(XML_TAG_HAFTPFLICHT_VERSICHERUNGS_TYP);
		if (associationElements != null) {
			Element element = associationElements.get(0);
			haftpflichtVersicherungsTyp = new ProductComponentLink<>(this);
			haftpflichtVersicherungsTyp.initFromXml(element);
		}
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitKaskoVersicherungsTyp(Map<String, List<Element>> elementsMap) {
		List<Element> associationElements = elementsMap.get(XML_TAG_KASKO_VERSICHERUNGS_TYP);
		if (associationElements != null) {
			Element element = associationElements.get(0);
			kaskoVersicherungsTyp = new ProductComponentLink<>(this);
			kaskoVersicherungsTyp.initFromXml(element);
		}
	}

	/**
	 * Creates a new KfzVertrag that is configured.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzVertrag createKfzVertrag() {
		KfzVertrag policy = new KfzVertrag(this);
		policy.initialize();
		return policy;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public KfzVertrag createPolicyComponent() {
		return createKfzVertrag();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IProductComponentLink<? extends IProductComponent> getLink(String linkName, IProductComponent target) {
		if ("FahrzeugTyp".equals(linkName)) {
			return getLinkForFahrzeugTyp((FahrzeugTyp) target);
		}
		if ("HaftpflichtVersicherungsTyp".equals(linkName)) {
			return getLinkForHaftpflichtVersicherungsTyp((HaftpflichtVersicherungsTyp) target);
		}
		if ("KaskoVersicherungsTyp".equals(linkName)) {
			return getLinkForKaskoVersicherungsTyp((KaskoVersicherungsTyp) target);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public List<IProductComponentLink<? extends IProductComponent>> getLinks() {
		List<IProductComponentLink<? extends IProductComponent>> list = new ArrayList<>();
		if (getLinkForFahrzeugTyp() != null) {
			list.add(getLinkForFahrzeugTyp());
		}
		if (getLinkForHaftpflichtVersicherungsTyp() != null) {
			list.add(getLinkForHaftpflichtVersicherungsTyp());
		}
		if (getLinkForKaskoVersicherungsTyp() != null) {
			list.add(getLinkForKaskoVersicherungsTyp());
		}
		return list;
	}

	/**
	 * Creates a new instance of KfzProduktBuilder to edit this product.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzProduktBuilder modify() {
		return KfzProduktBuilder.from(this, (InMemoryRuntimeRepository) this.getRepository());
	}

	/**
	 * Generates a new instance of KfzProdukt with a given
	 * {@link InMemoryRuntimeRepository}, ID, kindID and versionID. The new product
	 * is set to be valid from 1900/1/1.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KfzProduktBuilder builder(InMemoryRuntimeRepository runtimeRepository, String id, String kindId,
			String versionId) {
		KfzProdukt product = new KfzProdukt(runtimeRepository, id, kindId, versionId);
		product.setValidFrom(new DateTime(1900, 1, 1));
		runtimeRepository.putProductComponent(product);

		return new KfzProduktBuilder(product, runtimeRepository);
	}

	/**
	 * Generates a new instance of KfzProdukt with a given
	 * {@link InMemoryRuntimeRepository}, ID, kindID and versionID.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KfzProduktBuilder builder(InMemoryRuntimeRepository runtimeRepository, String id, String kindId,
			String versionId, DateTime validFrom) {
		KfzProdukt product = new KfzProdukt(runtimeRepository, id, kindId, versionId);
		product.setValidFrom(validFrom);
		runtimeRepository.putProductComponent(product);

		return new KfzProduktBuilder(product, runtimeRepository);
	}

	/**
	 * Generates a new instance of KfzProdukt with the ID of an existing product
	 * component.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KfzProduktBuilder builder(InMemoryRuntimeRepository runtimeRepository, String prodCmptId) {
		KfzProdukt product = (KfzProdukt) runtimeRepository.getProductComponent(prodCmptId);

		if (product == null) {
			throw new RuntimeException("No product component found with given ID!");
		} else {
			return KfzProduktBuilder.from(product, runtimeRepository);
		}
	}
}
