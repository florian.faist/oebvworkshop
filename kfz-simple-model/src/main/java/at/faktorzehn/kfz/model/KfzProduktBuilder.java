package at.faktorzehn.kfz.model;

import org.faktorips.runtime.InMemoryRuntimeRepository;
import java.util.List;
import org.faktorips.valueset.ValueSet;
import org.faktorips.runtime.CardinalityRange;
import org.faktorips.runtime.annotation.IpsGenerated;

/**
 * Implementation of KfzProduktBuilder. A KfzProduktBuilder provides all
 * functionalities that is needed to create a KfzProdukt. Note that this is only
 * for testing purpose. All instances of product have to be created in a new
 * {@link InMemoryRuntimeRepository} as Product Component in the runtime
 * repository can not be edited.
 *
 * @generated
 */
public class KfzProduktBuilder {
	/**
	 * @generated
	 */
	private final InMemoryRuntimeRepository runtimeRepository;

	/**
	 * @generated
	 */
	private final KfzProdukt kfzProdukt;

	/**
	 * Generates a new instance of KfzProduktBuilder with a given product. The
	 * product must exist in the given {@link InMemoryRuntimeRepository} that is not
	 * allowed to be null.
	 *
	 * @generated
	 */
	@IpsGenerated
	protected KfzProduktBuilder(KfzProdukt product, InMemoryRuntimeRepository runtimeRepository) {
		if (product == null || runtimeRepository == null) {
			throw new RuntimeException("Product and repository can not be null!");
		} else {
			runtimeRepository.getExistingProductComponent(product.getId());

			this.runtimeRepository = runtimeRepository;
			this.kfzProdukt = product;
		}
	}

	/**
	 * Sets the value of attribute name.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzProduktBuilder name(String name) {
		kfzProdukt.setName(name);
		return this;
	}

	/**
	 * Sets the value of attribute psBerechnungsFaktor.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzProduktBuilder psBerechnungsFaktor(Double psBerechnungsFaktor) {
		kfzProdukt.setPsBerechnungsFaktor(psBerechnungsFaktor);
		return this;
	}

	/**
	 * Sets the value of attribute erlaubteMarken.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzProduktBuilder erlaubteMarken(List<FahrzeugMarken> erlaubteMarken) {
		kfzProdukt.setErlaubteMarken(erlaubteMarken);
		return this;
	}

	/**
	 * Sets the default value for zahlweise.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzProduktBuilder zahlweiseDefault(Integer zahlweise) {
		kfzProdukt.setDefaultValueZahlweise(zahlweise);
		return this;
	}

	/**
	 * Sets the set of allowed values for the property zahlweise.
	 * 
	 * @throws ClassCastException if the type of value set does not match the
	 *                            property's configuration.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzProduktBuilder zahlweiseAllowedValues(ValueSet<Integer> zahlweise) {
		kfzProdukt.setAllowedValuesForZahlweise(zahlweise);
		return this;
	}

	/**
	 * @return {@link InMemoryRuntimeRepository} that is saved.
	 *
	 * @generated
	 */
	@IpsGenerated
	public InMemoryRuntimeRepository getRepository() {
		return this.runtimeRepository;
	}

	/**
	 * @return instance of KfzProdukt that is built.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzProdukt getResult() {
		return kfzProdukt;
	}

	/**
	 * Interne Methode
	 *
	 * @generated
	 */
	@IpsGenerated
	public static Class<?> getProductClass() {
		return KfzProdukt.class;
	}

	/**
	 * Creates a new KfzProduktBuilder with a given KfzProdukt and a runtime
	 * repository.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KfzProduktBuilder from(KfzProdukt product, InMemoryRuntimeRepository runtimeRepository) {
		return new KfzProduktBuilder(product, runtimeRepository);
	}

	/**
	 * Returns an {@link AssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AssociationBuilder} you
	 * get the builder of the target object for further processing. Use the method
	 * {@link #add()} if you want to create multiple target objects and always
	 * return to this builder instead of the target builder.
	 * 
	 * @see #add()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AssociationBuilder associate() {
		return new AssociationBuilder(this);
	}

	/**
	 * Returns an {@link AddAssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AddAssociationBuilder}
	 * you always could return to this builder for further processing. Use the
	 * method {@link #associate()} if you want to have the builder of the target
	 * object.
	 * 
	 * @see #associate()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AddAssociationBuilder add() {
		return new AddAssociationBuilder(this);
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * returns a builder for the target class.
	 *
	 * @generated
	 */
	public static class AssociationBuilder {

		/**
		 * @generated
		 */
		private KfzProduktBuilder productBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AssociationBuilder(KfzProduktBuilder productBuilder) {
			this.productBuilder = productBuilder;
		}

		/**
		 * Gets a instance of a subclass of FahrzeugTyp with the ID of the product
		 * component and sets it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends FahrzeugTypBuilder> T fahrzeugTyp(T targetBuilder) {
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Gets a instance of a subclass of FahrzeugTyp with the ID of the product
		 * component and sets it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends FahrzeugTypBuilder> T fahrzeugTyp(T targetBuilder, CardinalityRange cardinality) {
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * Gets a instance of FahrzeugTyp with the ID of the product component and sets
		 * it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder fahrzeugTyp(String productCmptId) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), productCmptId);
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Generates a new Instance of FahrzeugTyp with the IDs and sets it as
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder fahrzeugTyp(String id, String kindId, String versionId) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), id, kindId, versionId);
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Gets a instance of FahrzeugTyp with the ID of the product component and sets
		 * it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder fahrzeugTyp(String productCmptId, CardinalityRange cardinality) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), productCmptId);
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * Generates a new Instance of FahrzeugTyp with the IDs and sets it as
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugTypBuilder fahrzeugTyp(String id, String kindId, String versionId,
				CardinalityRange cardinality) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), id, kindId, versionId);
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * Gets a instance of a subclass of KaskoVersicherungsTyp with the ID of the
		 * product component and sets it as KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends KaskoVersicherungsTypBuilder> T kaskoVersicherungsTyp(T targetBuilder) {
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Gets a instance of a subclass of KaskoVersicherungsTyp with the ID of the
		 * product component and sets it as KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends KaskoVersicherungsTypBuilder> T kaskoVersicherungsTyp(T targetBuilder,
				CardinalityRange cardinality) {
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * Gets a instance of KaskoVersicherungsTyp with the ID of the product component
		 * and sets it as KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder kaskoVersicherungsTyp(String productCmptId) {
			KaskoVersicherungsTypBuilder targetBuilder = KaskoVersicherungsTyp.builder(getRepository(), productCmptId);
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Generates a new Instance of KaskoVersicherungsTyp with the IDs and sets it as
		 * KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder kaskoVersicherungsTyp(String id, String kindId, String versionId) {
			KaskoVersicherungsTypBuilder targetBuilder = KaskoVersicherungsTyp.builder(getRepository(), id, kindId,
					versionId);
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Gets a instance of KaskoVersicherungsTyp with the ID of the product component
		 * and sets it as KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder kaskoVersicherungsTyp(String productCmptId, CardinalityRange cardinality) {
			KaskoVersicherungsTypBuilder targetBuilder = KaskoVersicherungsTyp.builder(getRepository(), productCmptId);
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * Generates a new Instance of KaskoVersicherungsTyp with the IDs and sets it as
		 * KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungsTypBuilder kaskoVersicherungsTyp(String id, String kindId, String versionId,
				CardinalityRange cardinality) {
			KaskoVersicherungsTypBuilder targetBuilder = KaskoVersicherungsTyp.builder(getRepository(), id, kindId,
					versionId);
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * Gets a instance of a subclass of HaftpflichtVersicherungsTyp with the ID of
		 * the product component and sets it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends HaftpflichtVersicherungsTypBuilder> T haftpflichtVersicherungsTyp(T targetBuilder) {
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Gets a instance of a subclass of HaftpflichtVersicherungsTyp with the ID of
		 * the product component and sets it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends HaftpflichtVersicherungsTypBuilder> T haftpflichtVersicherungsTyp(T targetBuilder,
				CardinalityRange cardinality) {
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * Gets a instance of HaftpflichtVersicherungsTyp with the ID of the product
		 * component and sets it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungsTypBuilder haftpflichtVersicherungsTyp(String productCmptId) {
			HaftpflichtVersicherungsTypBuilder targetBuilder = HaftpflichtVersicherungsTyp.builder(getRepository(),
					productCmptId);
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Generates a new Instance of HaftpflichtVersicherungsTyp with the IDs and sets
		 * it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungsTypBuilder haftpflichtVersicherungsTyp(String id, String kindId,
				String versionId) {
			HaftpflichtVersicherungsTypBuilder targetBuilder = HaftpflichtVersicherungsTyp.builder(getRepository(), id,
					kindId, versionId);
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Gets a instance of HaftpflichtVersicherungsTyp with the ID of the product
		 * component and sets it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungsTypBuilder haftpflichtVersicherungsTyp(String productCmptId,
				CardinalityRange cardinality) {
			HaftpflichtVersicherungsTypBuilder targetBuilder = HaftpflichtVersicherungsTyp.builder(getRepository(),
					productCmptId);
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * Generates a new Instance of HaftpflichtVersicherungsTyp with the IDs and sets
		 * it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungsTypBuilder haftpflichtVersicherungsTyp(String id, String kindId,
				String versionId, CardinalityRange cardinality) {
			HaftpflichtVersicherungsTypBuilder targetBuilder = HaftpflichtVersicherungsTyp.builder(getRepository(), id,
					kindId, versionId);
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return targetBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected KfzProdukt getResult() {
			return productBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected InMemoryRuntimeRepository getRepository() {
			return productBuilder.getRepository();
		}
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * return the original KfzProduktBuilder.
	 *
	 * @generated
	 */
	public static class AddAssociationBuilder {

		/**
		 * @generated
		 */
		private KfzProduktBuilder productBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AddAssociationBuilder(KfzProduktBuilder productBuilder) {
			this.productBuilder = productBuilder;
		}

		/**
		 * Sets a existing instance of FahrzeugTyp as the target of the association
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder fahrzeugTyp(FahrzeugTyp targetProduct) {
			getResult().setFahrzeugTyp(targetProduct);
			return done();
		}

		/**
		 * Sets a existing instance of FahrzeugTyp as the target of the association
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder fahrzeugTyp(FahrzeugTyp targetProduct, CardinalityRange cardinality) {
			getResult().setFahrzeugTyp(targetProduct, cardinality);
			return done();
		}

		/**
		 * Gets a instance of a subclass of FahrzeugTyp with the ID of the product
		 * component and sets it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder fahrzeugTyp(FahrzeugTypBuilder targetBuilder) {
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Gets a instance of a subclass of FahrzeugTyp with the ID of the product
		 * component and sets it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder fahrzeugTyp(FahrzeugTypBuilder targetBuilder, CardinalityRange cardinality) {
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * Gets a instance of FahrzeugTyp with the ID of the product component and sets
		 * it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder fahrzeugTyp(String productCmptId) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), productCmptId);
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Generates a new Instance of FahrzeugTyp with the IDs and sets it as
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder fahrzeugTyp(String id, String kindId, String versionId) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), id, kindId, versionId);
			getResult().setFahrzeugTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Gets a instance of FahrzeugTyp with the ID of the product component and sets
		 * it as FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder fahrzeugTyp(String productCmptId, CardinalityRange cardinality) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), productCmptId);
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * Generates a new Instance of FahrzeugTyp with the IDs and sets it as
		 * FahrzeugTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder fahrzeugTyp(String id, String kindId, String versionId, CardinalityRange cardinality) {
			FahrzeugTypBuilder targetBuilder = FahrzeugTyp.builder(getRepository(), id, kindId, versionId);
			getResult().setFahrzeugTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * Sets a existing instance of KaskoVersicherungsTyp as the target of the
		 * association KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder kaskoVersicherungsTyp(KaskoVersicherungsTyp targetProduct) {
			getResult().setKaskoVersicherungsTyp(targetProduct);
			return done();
		}

		/**
		 * Sets a existing instance of KaskoVersicherungsTyp as the target of the
		 * association KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder kaskoVersicherungsTyp(KaskoVersicherungsTyp targetProduct,
				CardinalityRange cardinality) {
			getResult().setKaskoVersicherungsTyp(targetProduct, cardinality);
			return done();
		}

		/**
		 * Gets a instance of a subclass of KaskoVersicherungsTyp with the ID of the
		 * product component and sets it as KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder kaskoVersicherungsTyp(KaskoVersicherungsTypBuilder targetBuilder) {
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Gets a instance of a subclass of KaskoVersicherungsTyp with the ID of the
		 * product component and sets it as KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder kaskoVersicherungsTyp(KaskoVersicherungsTypBuilder targetBuilder,
				CardinalityRange cardinality) {
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * Gets a instance of KaskoVersicherungsTyp with the ID of the product component
		 * and sets it as KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder kaskoVersicherungsTyp(String productCmptId) {
			KaskoVersicherungsTypBuilder targetBuilder = KaskoVersicherungsTyp.builder(getRepository(), productCmptId);
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Generates a new Instance of KaskoVersicherungsTyp with the IDs and sets it as
		 * KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder kaskoVersicherungsTyp(String id, String kindId, String versionId) {
			KaskoVersicherungsTypBuilder targetBuilder = KaskoVersicherungsTyp.builder(getRepository(), id, kindId,
					versionId);
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Gets a instance of KaskoVersicherungsTyp with the ID of the product component
		 * and sets it as KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder kaskoVersicherungsTyp(String productCmptId, CardinalityRange cardinality) {
			KaskoVersicherungsTypBuilder targetBuilder = KaskoVersicherungsTyp.builder(getRepository(), productCmptId);
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * Generates a new Instance of KaskoVersicherungsTyp with the IDs and sets it as
		 * KaskoVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder kaskoVersicherungsTyp(String id, String kindId, String versionId,
				CardinalityRange cardinality) {
			KaskoVersicherungsTypBuilder targetBuilder = KaskoVersicherungsTyp.builder(getRepository(), id, kindId,
					versionId);
			getResult().setKaskoVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * Sets a existing instance of HaftpflichtVersicherungsTyp as the target of the
		 * association HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder haftpflichtVersicherungsTyp(HaftpflichtVersicherungsTyp targetProduct) {
			getResult().setHaftpflichtVersicherungsTyp(targetProduct);
			return done();
		}

		/**
		 * Sets a existing instance of HaftpflichtVersicherungsTyp as the target of the
		 * association HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder haftpflichtVersicherungsTyp(HaftpflichtVersicherungsTyp targetProduct,
				CardinalityRange cardinality) {
			getResult().setHaftpflichtVersicherungsTyp(targetProduct, cardinality);
			return done();
		}

		/**
		 * Gets a instance of a subclass of HaftpflichtVersicherungsTyp with the ID of
		 * the product component and sets it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder haftpflichtVersicherungsTyp(HaftpflichtVersicherungsTypBuilder targetBuilder) {
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Gets a instance of a subclass of HaftpflichtVersicherungsTyp with the ID of
		 * the product component and sets it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder haftpflichtVersicherungsTyp(HaftpflichtVersicherungsTypBuilder targetBuilder,
				CardinalityRange cardinality) {
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * Gets a instance of HaftpflichtVersicherungsTyp with the ID of the product
		 * component and sets it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder haftpflichtVersicherungsTyp(String productCmptId) {
			HaftpflichtVersicherungsTypBuilder targetBuilder = HaftpflichtVersicherungsTyp.builder(getRepository(),
					productCmptId);
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Generates a new Instance of HaftpflichtVersicherungsTyp with the IDs and sets
		 * it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder haftpflichtVersicherungsTyp(String id, String kindId, String versionId) {
			HaftpflichtVersicherungsTypBuilder targetBuilder = HaftpflichtVersicherungsTyp.builder(getRepository(), id,
					kindId, versionId);
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult());
			return done();
		}

		/**
		 * Gets a instance of HaftpflichtVersicherungsTyp with the ID of the product
		 * component and sets it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder haftpflichtVersicherungsTyp(String productCmptId, CardinalityRange cardinality) {
			HaftpflichtVersicherungsTypBuilder targetBuilder = HaftpflichtVersicherungsTyp.builder(getRepository(),
					productCmptId);
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * Generates a new Instance of HaftpflichtVersicherungsTyp with the IDs and sets
		 * it as HaftpflichtVersicherungsTyp.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzProduktBuilder haftpflichtVersicherungsTyp(String id, String kindId, String versionId,
				CardinalityRange cardinality) {
			HaftpflichtVersicherungsTypBuilder targetBuilder = HaftpflichtVersicherungsTyp.builder(getRepository(), id,
					kindId, versionId);
			getResult().setHaftpflichtVersicherungsTyp(targetBuilder.getResult(), cardinality);
			return done();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected KfzProduktBuilder done() {
			return productBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected KfzProdukt getResult() {
			return productBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected InMemoryRuntimeRepository getRepository() {
			return productBuilder.getRepository();
		}
	}

}
