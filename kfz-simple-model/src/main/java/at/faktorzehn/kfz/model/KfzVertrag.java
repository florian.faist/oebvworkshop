package at.faktorzehn.kfz.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.faktorips.runtime.IConfigurableModelObject;
import org.faktorips.valueset.IntegerRange;
import org.faktorips.runtime.ICopySupport;
import org.faktorips.runtime.IDeltaComputationOptions;
import org.faktorips.runtime.IDeltaSupport;
import org.faktorips.runtime.IModelObject;
import org.faktorips.runtime.IModelObjectDelta;
import org.faktorips.runtime.IModelObjectVisitor;
import org.faktorips.runtime.IObjectReferenceStore;
import org.faktorips.runtime.IProductComponent;
import org.faktorips.runtime.IRuntimeRepository;
import org.faktorips.runtime.IValidationContext;
import org.faktorips.runtime.IVisitorSupport;
import org.faktorips.runtime.MessageList;
import org.faktorips.runtime.annotation.IpsGenerated;
import org.faktorips.runtime.internal.AbstractModelObject;
import org.faktorips.runtime.internal.IpsStringUtils;
import org.faktorips.runtime.internal.ModelObjectDelta;
import org.faktorips.runtime.internal.ProductConfiguration;
import org.faktorips.runtime.internal.XmlCallback;
import org.faktorips.runtime.model.annotation.IpsAllowedValues;
import org.faktorips.runtime.model.annotation.IpsAttribute;
import org.faktorips.runtime.model.annotation.IpsAttributeSetter;
import org.faktorips.runtime.model.annotation.IpsAttributes;
import org.faktorips.runtime.model.annotation.IpsAssociations;
import org.faktorips.runtime.model.annotation.IpsConfiguredAttribute;
import org.faktorips.runtime.model.annotation.IpsAssociation;
import org.faktorips.runtime.model.type.AssociationKind;
import org.faktorips.runtime.model.annotation.IpsMatchingAssociation;
import org.faktorips.runtime.model.annotation.IpsInverseAssociation;
import org.faktorips.runtime.model.annotation.IpsAssociationAdder;
import org.faktorips.runtime.model.annotation.IpsConfiguredBy;
import org.faktorips.runtime.model.annotation.IpsDocumented;
import org.faktorips.runtime.model.annotation.IpsPolicyCmptType;
import org.faktorips.runtime.model.type.AttributeKind;
import org.faktorips.runtime.model.type.ValueSetKind;
import org.faktorips.valueset.OrderedValueSet;
import org.w3c.dom.Element;

/**
 * Implementation for KfzVertrag.
 * 
 * @since 1.0.0
 *
 * @generated
 */
@IpsPolicyCmptType(name = "KfzVertrag")
@IpsAttributes({ "id", "zahlweise", "preis", "createDate" })
@IpsAssociations({ "Fahrzeug", "HaftpflichtVersicherung", "KaskoVersicherung" })
@IpsConfiguredBy(KfzProdukt.class)
@IpsDocumented(bundleName = "at.faktorzehn.kfz.model.model-label-and-descriptions", defaultLocale = "de")
public class KfzVertrag extends AbstractModelObject
		implements IDeltaSupport, ICopySupport, IVisitorSupport, IConfigurableModelObject {

	/**
	 * The maximal multiplicity of the association with the role name Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final IntegerRange MAX_MULTIPLICITY_OF_FAHRZEUG = IntegerRange.valueOf(0, 1);
	/**
	 * The name of the association fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String ASSOCIATION_FAHRZEUG = "fahrzeug";
	/**
	 * The maximal multiplicity of the association with the role name
	 * HaftpflichtVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final IntegerRange MAX_MULTIPLICITY_OF_HAFTPFLICHT_VERSICHERUNG = IntegerRange.valueOf(0, 1);
	/**
	 * The name of the association haftpflichtVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String ASSOCIATION_HAFTPFLICHT_VERSICHERUNG = "haftpflichtVersicherung";
	/**
	 * The maximal multiplicity of the association with the role name
	 * KaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final IntegerRange MAX_MULTIPLICITY_OF_KASKO_VERSICHERUNG = IntegerRange.valueOf(0, 1);
	/**
	 * The name of the association kaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String ASSOCIATION_KASKO_VERSICHERUNG = "kaskoVersicherung";
	/**
	 * The name of the property zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_ZAHLWEISE = "zahlweise";
	/**
	 * Max allowed values for property zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final OrderedValueSet<Integer> MAX_ALLOWED_VALUES_FOR_ZAHLWEISE = new OrderedValueSet<>(false, null,
			Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(4), Integer.valueOf(12));
	/**
	 * The name of the property preis.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_PREIS = "preis";
	/**
	 * The name of the property createDate.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_CREATEDATE = "createDate";
	/**
	 * Member variable for id.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private String id = null;
	/**
	 * The name of the property id.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	public static final String PROPERTY_ID = "id";
	/**
	 * Member variable for zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private Integer zahlweise = null;

	/**
	 * Member variable for preis.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private BigDecimal preis = null;
	/**
	 * Member variable for createDate.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private LocalDateTime createDate = null;
	/**
	 * References the current product configuration.
	 *
	 * @generated
	 */
	private ProductConfiguration productConfiguration;

	/**
	 * Member variable for the association with the role name Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private Fahrzeug fahrzeug = null;

	/**
	 * Member variable for the association with the role name
	 * HaftpflichtVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private HaftpflichtVersicherung haftpflichtVersicherung = null;

	/**
	 * Member variable for the association with the role name KaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	private KaskoVersicherung kaskoVersicherung = null;

	/**
	 * Creates a new KfzVertrag.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzVertrag() {
		super();
		productConfiguration = new ProductConfiguration();
	}

	/**
	 * Creates a new KfzVertrag.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzVertrag(KfzProdukt productCmpt) {
		super();
		productConfiguration = new ProductConfiguration(productCmpt);
	}

	/**
	 * Returns the id.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttribute(name = "id", kind = AttributeKind.CHANGEABLE, valueSetKind = ValueSetKind.AllValues)
	@IpsGenerated
	public String getId() {
		return id;
	}

	/**
	 * Sets the value of attribute id.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttributeSetter("id")
	@IpsGenerated
	public void setId(String newValue) {
		this.id = newValue;
	}

	/**
	 * Returns the set of allowed values for the property zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAllowedValues("zahlweise")
	@IpsGenerated
	public OrderedValueSet<Integer> getAllowedValuesForZahlweise(IValidationContext context) {
		return getKfzProdukt().getAllowedValuesForZahlweise(context);
	}

	/**
	 * Returns the zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttribute(name = "zahlweise", kind = AttributeKind.CHANGEABLE, valueSetKind = ValueSetKind.Enum)
	@IpsConfiguredAttribute(changingOverTime = false)
	@IpsGenerated
	public Integer getZahlweise() {
		return zahlweise;
	}

	/**
	 * Sets the value of attribute zahlweise.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttributeSetter("zahlweise")
	@IpsGenerated
	public void setZahlweise(Integer newValue) {
		this.zahlweise = newValue;
	}

	/**
	 * Returns the preis.
	 * 
	 * @since 1.0.0
	 *
	 * @generated NOT
	 */
	@IpsAttribute(name = "preis", kind = AttributeKind.DERIVED_BY_EXPLICIT_METHOD_CALL, valueSetKind = ValueSetKind.AllValues)
	@IpsConfiguredAttribute(changingOverTime = false)
	public BigDecimal getPreis() {
		return preis = this.getKfzProdukt().berechneBeitrag(this);
	}

	/**
	 * Returns the createDate.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttribute(name = "createDate", kind = AttributeKind.CHANGEABLE, valueSetKind = ValueSetKind.AllValues)
	@IpsGenerated
	public LocalDateTime getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the value of attribute createDate.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAttributeSetter("createDate")
	@IpsGenerated
	public void setCreateDate(LocalDateTime newValue) {
		this.createDate = newValue;
	}

	/**
	 * Returns the referenced Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "Fahrzeug", pluralName = "Fahrzeuge", kind = AssociationKind.Composition, targetClass = Fahrzeug.class, min = 0, max = 1)
	@IpsMatchingAssociation(source = KfzProdukt.class, name = "FahrzeugTyp")
	@IpsInverseAssociation("KfzVertrag")
	@IpsGenerated
	public Fahrzeug getFahrzeug() {
		return fahrzeug;
	}

	/**
	 * Sets the Fahrzeug.
	 * 
	 * @throws ClassCastException If the association is constrained and the given
	 *                            object is not of the correct type.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "Fahrzeug")
	@IpsGenerated
	public void setFahrzeug(Fahrzeug newObject) {
		if (fahrzeug != null) {
			fahrzeug.setKfzVertragInternal(null);
		}
		if (newObject != null) {
			newObject.setKfzVertragInternal(this);
		}
		fahrzeug = newObject;
	}

	/**
	 * Creates a new Fahrzeug and adds it as Fahrzeug.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public Fahrzeug newFahrzeug() {
		Fahrzeug newFahrzeug = new Fahrzeug();
		setFahrzeug(newFahrzeug);
		newFahrzeug.initialize();
		return newFahrzeug;
	}

	/**
	 * Creates a new Fahrzeug and adds it as Fahrzeug.
	 * 
	 * @param fahrzeugTyp The product component that configures the new object.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public Fahrzeug newFahrzeug(FahrzeugTyp fahrzeugTyp) {
		if (fahrzeugTyp == null) {
			return newFahrzeug();
		}
		Fahrzeug newFahrzeug = fahrzeugTyp.createFahrzeug();
		setFahrzeug(newFahrzeug);
		newFahrzeug.initialize();
		return newFahrzeug;
	}

	/**
	 * Returns the referenced HaftpflichtVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "HaftpflichtVersicherung", pluralName = "HaftpflichtVersicherungen", kind = AssociationKind.Composition, targetClass = HaftpflichtVersicherung.class, min = 0, max = 1)
	@IpsMatchingAssociation(source = KfzProdukt.class, name = "HaftpflichtVersicherungsTyp")
	@IpsInverseAssociation("KfzVertrag")
	@IpsGenerated
	public HaftpflichtVersicherung getHaftpflichtVersicherung() {
		return haftpflichtVersicherung;
	}

	/**
	 * Sets the HaftpflichtVersicherung.
	 * 
	 * @throws ClassCastException If the association is constrained and the given
	 *                            object is not of the correct type.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "HaftpflichtVersicherung")
	@IpsGenerated
	public void setHaftpflichtVersicherung(HaftpflichtVersicherung newObject) {
		if (haftpflichtVersicherung != null) {
			haftpflichtVersicherung.setKfzVertragInternal(null);
		}
		if (newObject != null) {
			newObject.setKfzVertragInternal(this);
		}
		haftpflichtVersicherung = newObject;
	}

	/**
	 * Creates a new HaftpflichtVersicherung and adds it as HaftpflichtVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public HaftpflichtVersicherung newHaftpflichtVersicherung() {
		HaftpflichtVersicherung newHaftpflichtVersicherung = new HaftpflichtVersicherung();
		setHaftpflichtVersicherung(newHaftpflichtVersicherung);
		newHaftpflichtVersicherung.initialize();
		return newHaftpflichtVersicherung;
	}

	/**
	 * Creates a new HaftpflichtVersicherung and adds it as HaftpflichtVersicherung.
	 * 
	 * @param haftpflichtVersicherungsTyp The product component that configures the
	 *                                    new object.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public HaftpflichtVersicherung newHaftpflichtVersicherung(HaftpflichtVersicherungsTyp haftpflichtVersicherungsTyp) {
		if (haftpflichtVersicherungsTyp == null) {
			return newHaftpflichtVersicherung();
		}
		HaftpflichtVersicherung newHaftpflichtVersicherung = haftpflichtVersicherungsTyp
				.createHaftpflichtVersicherung();
		setHaftpflichtVersicherung(newHaftpflichtVersicherung);
		newHaftpflichtVersicherung.initialize();
		return newHaftpflichtVersicherung;
	}

	/**
	 * Returns the referenced KaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociation(name = "KaskoVersicherung", pluralName = "KaskoVersicherungen", kind = AssociationKind.Composition, targetClass = KaskoVersicherung.class, min = 0, max = 1)
	@IpsMatchingAssociation(source = KfzProdukt.class, name = "KaskoVersicherungsTyp")
	@IpsInverseAssociation("KfzVertrag")
	@IpsGenerated
	public KaskoVersicherung getKaskoVersicherung() {
		return kaskoVersicherung;
	}

	/**
	 * Sets the KaskoVersicherung.
	 * 
	 * @throws ClassCastException If the association is constrained and the given
	 *                            object is not of the correct type.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsAssociationAdder(association = "KaskoVersicherung")
	@IpsGenerated
	public void setKaskoVersicherung(KaskoVersicherung newObject) {
		if (kaskoVersicherung != null) {
			kaskoVersicherung.setKfzVertragInternal(null);
		}
		if (newObject != null) {
			newObject.setKfzVertragInternal(this);
		}
		kaskoVersicherung = newObject;
	}

	/**
	 * Creates a new KaskoVersicherung and adds it as KaskoVersicherung.
	 * 
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public KaskoVersicherung newKaskoVersicherung() {
		KaskoVersicherung newKaskoVersicherung = new KaskoVersicherung();
		setKaskoVersicherung(newKaskoVersicherung);
		newKaskoVersicherung.initialize();
		return newKaskoVersicherung;
	}

	/**
	 * Creates a new KaskoVersicherung and adds it as KaskoVersicherung.
	 * 
	 * @param kaskoVersicherungsTyp The product component that configures the new
	 *                              object.
	 * @since 1.0.0
	 *
	 * @generated
	 */
	@IpsGenerated
	public KaskoVersicherung newKaskoVersicherung(KaskoVersicherungsTyp kaskoVersicherungsTyp) {
		if (kaskoVersicherungsTyp == null) {
			return newKaskoVersicherung();
		}
		KaskoVersicherung newKaskoVersicherung = kaskoVersicherungsTyp.createKaskoVersicherung();
		setKaskoVersicherung(newKaskoVersicherung);
		newKaskoVersicherung.initialize();
		return newKaskoVersicherung;
	}

	/**
	 * Initializes the object with the configured defaults.
	 *
	 * @restrainedmodifiable
	 */
	@Override
	@IpsGenerated
	public void initialize() {
		if (getKfzProdukt() != null) {
			setZahlweise(getKfzProdukt().getDefaultValueZahlweise());
		}
		// begin-user-code
		this.id = UUID.randomUUID().toString();
		this.createDate = LocalDateTime.now();
		// end-user-code
	}

	/**
	 * Returns the KfzProdukt that configures this object.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzProdukt getKfzProdukt() {
		return (KfzProdukt) getProductComponent();
	}

	/**
	 * Sets the new KfzProdukt that configures this object.
	 * 
	 * @param kfzProdukt                             The new KfzProdukt.
	 * @param initPropertiesWithConfiguratedDefaults <code>true</code> if the
	 *                                               properties should be
	 *                                               initialized with the defaults
	 *                                               defined in the KfzProdukt.
	 *
	 * @generated
	 */
	@IpsGenerated
	public void setKfzProdukt(KfzProdukt kfzProdukt, boolean initPropertiesWithConfiguratedDefaults) {
		setProductComponent(kfzProdukt);
		if (initPropertiesWithConfiguratedDefaults) {
			initialize();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IProductComponent getProductComponent() {
		return productConfiguration.getProductComponent();
	}

	/**
	 * Sets the current product component.
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public void setProductComponent(IProductComponent productComponent) {
		productConfiguration.setProductComponent(productComponent);
	}

	/**
	 * This method is called when the effective from date has changed, so that the
	 * reference to the product component generation can be cleared. If this policy
	 * component contains child components, this method will also clear the
	 * reference to their product component generations.
	 * <p>
	 * The product component generation is cleared if and only if there is a new
	 * effective from date. If {@link #getEffectiveFromAsCalendar()} returns
	 * <code>null</code> the product component generation is not reset, for example
	 * if this model object was removed from its parent.
	 * <p>
	 * Clients may change the behavior of resetting the product component by
	 * overwriting {@link #resetProductCmptGenerationAfterEffectiveFromHasChanged()}
	 * instead of this method.
	 *
	 * @generated
	 */
	@IpsGenerated
	public void effectiveFromHasChanged() {
		if (getEffectiveFromAsCalendar() != null) {
			resetProductCmptGenerationAfterEffectiveFromHasChanged();
		}
		if (fahrzeug != null) {
			fahrzeug.effectiveFromHasChanged();
		}
		if (haftpflichtVersicherung != null) {
			haftpflichtVersicherung.effectiveFromHasChanged();
		}
		if (kaskoVersicherung != null) {
			kaskoVersicherung.effectiveFromHasChanged();
		}
	}

	/**
	 * Clears the product component generation.
	 * <p>
	 * This method can be overwritten to affect the behavior in case of an
	 * effective-date change.
	 *
	 * @generated
	 */
	@IpsGenerated
	protected void resetProductCmptGenerationAfterEffectiveFromHasChanged() {
		productConfiguration.resetProductCmptGeneration();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public Calendar getEffectiveFromAsCalendar() {
		// TODO Implement access to effective from.
		// To avoid that the generator overwrites the implementation,
		// you have to add NOT after @annotation in the Javadoc!
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void initFromXml(Element objectEl, boolean initWithProductDefaultsBeforeReadingXmlData,
			IRuntimeRepository productRepository, IObjectReferenceStore store, XmlCallback xmlCallback,
			String currPath) {
		productConfiguration.initFromXml(objectEl, productRepository);
		if (initWithProductDefaultsBeforeReadingXmlData) {
			initialize();
		}
		super.initFromXml(objectEl, initWithProductDefaultsBeforeReadingXmlData, productRepository, store, xmlCallback,
				currPath);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected void initPropertiesFromXml(Map<String, String> propMap, IRuntimeRepository productRepository) {
		super.initPropertiesFromXml(propMap, productRepository);
		doInitId(propMap);
		doInitZahlweise(propMap);
		doInitPreis(propMap);
		doInitCreateDate(propMap);
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitId(Map<String, String> propMap) {
		if (propMap.containsKey(PROPERTY_ID)) {
			this.id = propMap.get(PROPERTY_ID);
		}
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitZahlweise(Map<String, String> propMap) {
		if (propMap.containsKey(PROPERTY_ZAHLWEISE)) {
			this.zahlweise = IpsStringUtils.isEmpty(propMap.get(PROPERTY_ZAHLWEISE)) ? null
					: Integer.valueOf(propMap.get(PROPERTY_ZAHLWEISE));
		}
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitPreis(Map<String, String> propMap) {
		if (propMap.containsKey(PROPERTY_PREIS)) {
			this.preis = IpsStringUtils.isEmpty(propMap.get(PROPERTY_PREIS)) ? null
					: new BigDecimal(propMap.get(PROPERTY_PREIS));
		}
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private void doInitCreateDate(Map<String, String> propMap) {
		if (propMap.containsKey(PROPERTY_CREATEDATE)) {
			this.createDate = IpsStringUtils.isEmpty(propMap.get(PROPERTY_CREATEDATE)) ? null
					: LocalDateTime.parse(propMap.get(PROPERTY_CREATEDATE));
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	protected AbstractModelObject createChildFromXml(Element childEl) {
		AbstractModelObject newChild = super.createChildFromXml(childEl);
		if (newChild != null) {
			return newChild;
		}
		if ("Fahrzeug".equals(childEl.getNodeName())) {
			return doInitFahrzeug(childEl);
		}
		if ("HaftpflichtVersicherung".equals(childEl.getNodeName())) {
			return doInitHaftpflichtVersicherung(childEl);
		}
		if ("KaskoVersicherung".equals(childEl.getNodeName())) {
			return doInitKaskoVersicherung(childEl);
		}
		return null;
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private AbstractModelObject doInitFahrzeug(Element childEl) {
		String className = childEl.getAttribute("class");
		if (className.length() > 0) {
			try {
				Fahrzeug fahrzeugLocalVar = (Fahrzeug) Class.forName(className).getConstructor().newInstance();
				setFahrzeug(fahrzeugLocalVar);
				return fahrzeugLocalVar;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return newFahrzeug();
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private AbstractModelObject doInitHaftpflichtVersicherung(Element childEl) {
		String className = childEl.getAttribute("class");
		if (className.length() > 0) {
			try {
				HaftpflichtVersicherung haftpflichtVersicherungLocalVar = (HaftpflichtVersicherung) Class
						.forName(className).getConstructor().newInstance();
				setHaftpflichtVersicherung(haftpflichtVersicherungLocalVar);
				return haftpflichtVersicherungLocalVar;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return newHaftpflichtVersicherung();
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	private AbstractModelObject doInitKaskoVersicherung(Element childEl) {
		String className = childEl.getAttribute("class");
		if (className.length() > 0) {
			try {
				KaskoVersicherung kaskoVersicherungLocalVar = (KaskoVersicherung) Class.forName(className)
						.getConstructor().newInstance();
				setKaskoVersicherung(kaskoVersicherungLocalVar);
				return kaskoVersicherungLocalVar;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return newKaskoVersicherung();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public IModelObjectDelta computeDelta(IModelObject otherObject, IDeltaComputationOptions options) {
		ModelObjectDelta delta = ModelObjectDelta.newDelta(this, otherObject, options);
		if (!KfzVertrag.class.isAssignableFrom(otherObject.getClass())) {
			return delta;
		}
		KfzVertrag otherKfzVertrag = (KfzVertrag) otherObject;
		delta.checkPropertyChange(KfzVertrag.PROPERTY_ID, id, otherKfzVertrag.id, options);
		delta.checkPropertyChange(KfzVertrag.PROPERTY_ZAHLWEISE, zahlweise, otherKfzVertrag.zahlweise, options);
		delta.checkPropertyChange(KfzVertrag.PROPERTY_CREATEDATE, createDate, otherKfzVertrag.createDate, options);
		ModelObjectDelta.createChildDeltas(delta, fahrzeug, otherKfzVertrag.fahrzeug, ASSOCIATION_FAHRZEUG, options);
		ModelObjectDelta.createChildDeltas(delta, haftpflichtVersicherung, otherKfzVertrag.haftpflichtVersicherung,
				ASSOCIATION_HAFTPFLICHT_VERSICHERUNG, options);
		ModelObjectDelta.createChildDeltas(delta, kaskoVersicherung, otherKfzVertrag.kaskoVersicherung,
				ASSOCIATION_KASKO_VERSICHERUNG, options);
		return delta;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public KfzVertrag newCopy() {
		Map<IModelObject, IModelObject> copyMap = new HashMap<>();
		KfzVertrag newCopy = newCopyInternal(copyMap);
		copyAssociationsInternal(newCopy, copyMap);
		return newCopy;
	}

	/**
	 * Internal copy method with a {@link Map} containing already copied instances.
	 * 
	 * @param copyMap the map contains the copied instances
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzVertrag newCopyInternal(Map<IModelObject, IModelObject> copyMap) {
		KfzVertrag newCopy = (KfzVertrag) copyMap.get(this);
		if (newCopy == null) {
			newCopy = new KfzVertrag();
			copyMap.put(this, newCopy);
			newCopy.copyProductCmptAndGenerationInternal(this);
			copyProperties(newCopy, copyMap);
		}
		return newCopy;
	}

	/**
	 * Copies the product component and product component generation from the other
	 * object.
	 *
	 * @generated
	 */
	@IpsGenerated
	protected void copyProductCmptAndGenerationInternal(KfzVertrag otherObject) {
		productConfiguration.copy(otherObject.productConfiguration);
	}

	/**
	 * This method sets all properties in the copy with the values of this object.
	 * If there are copied associated objects they are added to the copyMap in
	 * {@link #newCopyInternal(Map)}.
	 * 
	 * @param copy    The copy object
	 * @param copyMap a map containing copied associated objects
	 *
	 * @generated
	 */
	@IpsGenerated
	protected void copyProperties(IModelObject copy, Map<IModelObject, IModelObject> copyMap) {
		KfzVertrag concreteCopy = (KfzVertrag) copy;
		concreteCopy.id = id;
		concreteCopy.zahlweise = zahlweise;
		concreteCopy.preis = preis;
		concreteCopy.createDate = createDate;
		if (fahrzeug != null) {
			concreteCopy.fahrzeug = fahrzeug.newCopyInternal(copyMap);
			concreteCopy.fahrzeug.setKfzVertragInternal(concreteCopy);
		}
		if (haftpflichtVersicherung != null) {
			concreteCopy.haftpflichtVersicherung = haftpflichtVersicherung.newCopyInternal(copyMap);
			concreteCopy.haftpflichtVersicherung.setKfzVertragInternal(concreteCopy);
		}
		if (kaskoVersicherung != null) {
			concreteCopy.kaskoVersicherung = kaskoVersicherung.newCopyInternal(copyMap);
			concreteCopy.kaskoVersicherung.setKfzVertragInternal(concreteCopy);
		}
	}

	/**
	 * Internal method for setting copied associations. For copied targets, the
	 * associations have to be retargeted to the new copied instances. This method
	 * have to call {@link #copyAssociationsInternal(IModelObject, Map)} in other
	 * instances associated by composite.
	 * 
	 * @param abstractCopy the copy of this policy component
	 * @param copyMap      the map contains the copied instances
	 *
	 * @generated
	 */
	@IpsGenerated
	public void copyAssociationsInternal(IModelObject abstractCopy, Map<IModelObject, IModelObject> copyMap) {
		if (fahrzeug != null) {
			Fahrzeug copyFahrzeug = (Fahrzeug) copyMap.get(fahrzeug);
			fahrzeug.copyAssociationsInternal(copyFahrzeug, copyMap);
		}
		if (haftpflichtVersicherung != null) {
			HaftpflichtVersicherung copyHaftpflichtVersicherung = (HaftpflichtVersicherung) copyMap
					.get(haftpflichtVersicherung);
			haftpflichtVersicherung.copyAssociationsInternal(copyHaftpflichtVersicherung, copyMap);
		}
		if (kaskoVersicherung != null) {
			KaskoVersicherung copyKaskoVersicherung = (KaskoVersicherung) copyMap.get(kaskoVersicherung);
			kaskoVersicherung.copyAssociationsInternal(copyKaskoVersicherung, copyMap);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public boolean accept(IModelObjectVisitor visitor) {
		if (!visitor.visit(this)) {
			return false;
		}
		if (fahrzeug != null) {
			fahrzeug.accept(visitor);
		}
		if (haftpflichtVersicherung != null) {
			haftpflichtVersicherung.accept(visitor);
		}
		if (kaskoVersicherung != null) {
			kaskoVersicherung.accept(visitor);
		}
		return true;
	}

	/**
	 * Validates the object (but not its children). Returns <code>true</code> if
	 * this object should continue validating, <code>false</code> otherwise.
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public boolean validateSelf(MessageList ml, IValidationContext context) {
		if (!super.validateSelf(ml, context)) {
			return STOP_VALIDATION;
		}
		return CONTINUE_VALIDATION;
	}

	/**
	 * Validates the object's children.
	 *
	 * @generated
	 */
	@Override
	@IpsGenerated
	public void validateDependants(MessageList ml, IValidationContext context) {
		super.validateDependants(ml, context);
		if (fahrzeug != null) {
			ml.add(fahrzeug.validate(context));
		}
		if (haftpflichtVersicherung != null) {
			ml.add(haftpflichtVersicherung.validate(context));
		}
		if (kaskoVersicherung != null) {
			ml.add(kaskoVersicherung.validate(context));
		}
	}

	/**
	 * @restrainedmodifiable
	 */
	@Override
	@IpsGenerated
	public String toString() {
		// begin-user-code
		return getProductComponent() == null ? getClass().getSimpleName()
				: getClass().getSimpleName() + '[' + getProductComponent().toString() + ']';
		// end-user-code
	}

	/**
	 * Creates a new instance of KfzVertragBuilder to edit this policy.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzVertragBuilder modify() {
		return KfzVertragBuilder.from(this, getProductComponent().getRepository());
	}

	/**
	 * The runtime repository is used to create configured association targets from
	 * existing product components.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzVertragBuilder modify(IRuntimeRepository runtimeRepository) {
		return KfzVertragBuilder.from(this, runtimeRepository);
	}

	/**
	 * Creates a new KfzVertragBuilder with a new instance of policy. Runtime
	 * repository is set to null.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KfzVertragBuilder builder() {
		return KfzVertragBuilder.from(new KfzVertrag(), null);
	}

	/**
	 * Creates a new KfzVertragBuilder with a new instance of policy. Runtime
	 * repository is set to null. The runtime repository is required as association
	 * targets exists that are configured by a product. The product components of
	 * the targets must be in the given runtime repository.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KfzVertragBuilder builder(IRuntimeRepository runtimeRepository) {
		return KfzVertragBuilder.from(new KfzVertrag(), runtimeRepository);
	}

	/**
	 * Creates a new KfzVertragBuilder with a new instance of policy created by the
	 * given product component.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KfzVertragBuilder builder(KfzProdukt productCmpt) {
		return KfzVertragBuilder.from(new KfzVertrag(productCmpt), productCmpt.getRepository());
	}

	/**
	 * Creates a new KfzVertragBuilder with a new instance of policy created by the
	 * product component with the given ID in the runtime repository that configures
	 * the policy.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KfzVertragBuilder builder(IRuntimeRepository runtimeRepository, String productCmptId) {
		KfzProdukt product = (KfzProdukt) runtimeRepository.getProductComponent(productCmptId);
		if (product == null) {
			throw new RuntimeException("No product component found with given ID!");
		} else {
			KfzVertrag policy = product.createKfzVertrag();

			policy.initialize();
			return KfzVertragBuilder.from(policy, runtimeRepository);
		}
	}

}
