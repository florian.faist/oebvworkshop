package at.faktorzehn.kfz.model;

import org.faktorips.runtime.IRuntimeRepository;
import java.time.LocalDateTime;
import org.faktorips.runtime.annotation.IpsGenerated;

/**
 * This class provides methods to create and edit a KfzVertrag.
 *
 * @generated
 */
public class KfzVertragBuilder {
	/**
	 * @generated
	 */
	private final KfzVertrag kfzVertrag;

	/**
	 * @generated
	 */
	private IRuntimeRepository runtimeRepository;

	/**
	 * Creates a new KfzVertragBuilder with a given policy and runtime repository.
	 * This constructor is only to be used internally by the builder!
	 *
	 * @generated
	 */
	@IpsGenerated
	protected KfzVertragBuilder(KfzVertrag policy, IRuntimeRepository runtimeRepository) {
		this.kfzVertrag = policy;
		this.runtimeRepository = runtimeRepository;
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	public KfzVertragBuilder setRepository(IRuntimeRepository runtimeRepository) {
		this.runtimeRepository = runtimeRepository;
		return this;
	}

	/**
	 * Sets the value of attribute id.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzVertragBuilder id(String newId) {
		kfzVertrag.setId(newId);
		return this;
	}

	/**
	 * Sets the value of attribute zahlweise.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzVertragBuilder zahlweise(Integer newZahlweise) {
		kfzVertrag.setZahlweise(newZahlweise);
		return this;
	}

	/**
	 * Sets the value of attribute createDate.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzVertragBuilder createDate(LocalDateTime newCreateDate) {
		kfzVertrag.setCreateDate(newCreateDate);
		return this;
	}

	/**
	 * Returns the KfzVertrag that is built by this instance.
	 *
	 * @generated
	 */
	@IpsGenerated
	public KfzVertrag getResult() {
		return kfzVertrag;
	}

	/**
	 * @generated
	 */
	@IpsGenerated
	public IRuntimeRepository getRepository() {
		return runtimeRepository;
	}

	/**
	 * 
	 *
	 * @generated
	 */
	@IpsGenerated
	public static Class<?> getPolicyClass() {
		return KfzVertrag.class;
	}

	/**
	 * Creates a new KfzVertragBuilder with a given KfzVertrag.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KfzVertragBuilder from(KfzVertrag policy) {
		return new KfzVertragBuilder(policy, null);
	}

	/**
	 * Creates a new KfzVertragBuilder with a given KfzVertrag and a runtime
	 * repository.
	 *
	 * @generated
	 */
	@IpsGenerated
	public static KfzVertragBuilder from(KfzVertrag policy, IRuntimeRepository runtimeRepository) {
		return new KfzVertragBuilder(policy, runtimeRepository);
	}

	/**
	 * Returns an {@link AssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AssociationBuilder} you
	 * get the builder of the target object for further processing. Use the method
	 * {@link #add()} if you want to create multiple target objects and always
	 * return to this builder instead of the target builder.
	 * 
	 * @see #add()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AssociationBuilder associate() {
		return new AssociationBuilder(this);
	}

	/**
	 * Returns an {@link AddAssociationBuilder} to build a target object and add
	 * directly to a specified association. With the {@link AddAssociationBuilder}
	 * you always could return to this builder for further processing. Use the
	 * method {@link #associate()} if you want to have the builder of the target
	 * object.
	 * 
	 * @see #associate()
	 *
	 * @generated
	 */
	@IpsGenerated
	public AddAssociationBuilder add() {
		return new AddAssociationBuilder(this);
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * returns a builder for the target class.
	 *
	 * @generated
	 */
	public static class AssociationBuilder {

		/**
		 * @generated
		 */
		private KfzVertragBuilder policyBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AssociationBuilder(KfzVertragBuilder policyBuilder) {
			this.policyBuilder = policyBuilder;
		}

		/**
		 * Creates a new instance of a subclass of Fahrzeug with the given builder and
		 * sets it as the target of the association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends FahrzeugBuilder> T fahrzeug(T targetBuilder) {
			getResult().setFahrzeug(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of Fahrzeug and set it as the target of the
		 * association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder fahrzeug() {
			FahrzeugBuilder targetBuilder = Fahrzeug.builder();
			getResult().setFahrzeug(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of Fahrzeug from a given product component and sets it
		 * as the target of the association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public FahrzeugBuilder fahrzeug(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			FahrzeugBuilder targetBuilder = null;
			targetBuilder = Fahrzeug.builder(getRepository(), productCmptId);
			getResult().setFahrzeug(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of a subclass of HaftpflichtVersicherung with the
		 * given builder and sets it as the target of the association
		 * HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends HaftpflichtVersicherungBuilder> T haftpflichtVersicherung(T targetBuilder) {
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of HaftpflichtVersicherung and set it as the target of
		 * the association HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungBuilder haftpflichtVersicherung() {
			HaftpflichtVersicherungBuilder targetBuilder = HaftpflichtVersicherung.builder();
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of HaftpflichtVersicherung from a given product
		 * component and sets it as the target of the association
		 * HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public HaftpflichtVersicherungBuilder haftpflichtVersicherung(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			HaftpflichtVersicherungBuilder targetBuilder = null;
			targetBuilder = HaftpflichtVersicherung.builder(getRepository(), productCmptId);
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of a subclass of KaskoVersicherung with the given
		 * builder and sets it as the target of the association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public <T extends KaskoVersicherungBuilder> T kaskoVersicherung(T targetBuilder) {
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of KaskoVersicherung and set it as the target of the
		 * association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungBuilder kaskoVersicherung() {
			KaskoVersicherungBuilder targetBuilder = KaskoVersicherung.builder();
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * Creates a new instance of KaskoVersicherung from a given product component
		 * and sets it as the target of the association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KaskoVersicherungBuilder kaskoVersicherung(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			KaskoVersicherungBuilder targetBuilder = null;
			targetBuilder = KaskoVersicherung.builder(getRepository(), productCmptId);
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return targetBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected KfzVertrag getResult() {
			return policyBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected IRuntimeRepository getRepository() {
			return policyBuilder.getRepository();
		}
	}

	/**
	 * This class wraps setter methods for associations. Methods in this class
	 * return the original KfzVertragBuilder.
	 *
	 * @generated
	 */
	public static class AddAssociationBuilder {

		/**
		 * @generated
		 */
		private KfzVertragBuilder policyBuilder;

		/**
		 * @generated
		 */
		@IpsGenerated
		protected AddAssociationBuilder(KfzVertragBuilder policyBuilder) {
			this.policyBuilder = policyBuilder;
		}

		/**
		 * Set a existing instance of Fahrzeug as the target of the association
		 * Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder fahrzeug(Fahrzeug targetPolicy) {
			getResult().setFahrzeug(targetPolicy);
			return done();
		}

		/**
		 * Creates a new instance of a subclass of Fahrzeug with the given builder and
		 * sets it as the target of the association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder fahrzeug(FahrzeugBuilder targetBuilder) {
			getResult().setFahrzeug(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of Fahrzeug and set it as the target of the
		 * association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder fahrzeug() {
			FahrzeugBuilder targetBuilder = Fahrzeug.builder();
			getResult().setFahrzeug(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of Fahrzeug from a given product component and sets it
		 * as the target of the association Fahrzeug.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder fahrzeug(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			FahrzeugBuilder targetBuilder = null;
			targetBuilder = Fahrzeug.builder(getRepository(), productCmptId);
			getResult().setFahrzeug(targetBuilder.getResult());
			return done();
		}

		/**
		 * Set a existing instance of HaftpflichtVersicherung as the target of the
		 * association HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder haftpflichtVersicherung(HaftpflichtVersicherung targetPolicy) {
			getResult().setHaftpflichtVersicherung(targetPolicy);
			return done();
		}

		/**
		 * Creates a new instance of a subclass of HaftpflichtVersicherung with the
		 * given builder and sets it as the target of the association
		 * HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder haftpflichtVersicherung(HaftpflichtVersicherungBuilder targetBuilder) {
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of HaftpflichtVersicherung and set it as the target of
		 * the association HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder haftpflichtVersicherung() {
			HaftpflichtVersicherungBuilder targetBuilder = HaftpflichtVersicherung.builder();
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of HaftpflichtVersicherung from a given product
		 * component and sets it as the target of the association
		 * HaftpflichtVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder haftpflichtVersicherung(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			HaftpflichtVersicherungBuilder targetBuilder = null;
			targetBuilder = HaftpflichtVersicherung.builder(getRepository(), productCmptId);
			getResult().setHaftpflichtVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * Set a existing instance of KaskoVersicherung as the target of the association
		 * KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder kaskoVersicherung(KaskoVersicherung targetPolicy) {
			getResult().setKaskoVersicherung(targetPolicy);
			return done();
		}

		/**
		 * Creates a new instance of a subclass of KaskoVersicherung with the given
		 * builder and sets it as the target of the association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder kaskoVersicherung(KaskoVersicherungBuilder targetBuilder) {
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of KaskoVersicherung and set it as the target of the
		 * association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder kaskoVersicherung() {
			KaskoVersicherungBuilder targetBuilder = KaskoVersicherung.builder();
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * Creates a new instance of KaskoVersicherung from a given product component
		 * and sets it as the target of the association KaskoVersicherung.
		 *
		 * @generated
		 */
		@IpsGenerated
		public KfzVertragBuilder kaskoVersicherung(String productCmptId) {
			if (getRepository() == null) {
				throw new RuntimeException("No repository given! ");
			}
			KaskoVersicherungBuilder targetBuilder = null;
			targetBuilder = KaskoVersicherung.builder(getRepository(), productCmptId);
			getResult().setKaskoVersicherung(targetBuilder.getResult());
			return done();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected KfzVertragBuilder done() {
			return policyBuilder;
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected KfzVertrag getResult() {
			return policyBuilder.getResult();
		}

		/**
		 * @generated
		 */
		@IpsGenerated
		protected IRuntimeRepository getRepository() {
			return policyBuilder.getRepository();
		}
	}

}
