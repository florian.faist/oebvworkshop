package at.faktorzehn.export;

import org.faktorips.runtime.IProductComponent;
import org.faktorips.runtime.IRuntimeRepository;
import org.faktorips.runtime.internal.AbstractRuntimeRepository;

import at.faktorzehn.kfz.model.KfzProdukt;

//import at.faktorzehn.kfz.model.kfz.KfzProdukt;

public class KfzProductRepository extends KfzModelRepository {

	public KfzProductRepository() {
		super();
	}

	public KfzProductRepository(ClassLoader clazzloader) {
		super(clazzloader);
	}

	private IRuntimeRepository repository;
	private KfzProdukt kompaktProdukt;

	public KfzProdukt getKfzProduct() {
		if (kompaktProdukt == null) {
			repository = createKfzRuntimeRepository();

			// Referenz auf das Kompaktprodukt aus dem Repository holen
			IProductComponent pc = repository.getProductComponent("kfz.produkte.KfzOnline 2022-01");

			// Auf die eigenen Modellklassen casten
			kompaktProdukt = (KfzProdukt) pc;
		}
		return kompaktProdukt;
	}

	/**
	 * 
	 * Erzeugt {@link AbstractRuntimeRepository}, das alle Repositories der
	 * KFZ-Versicherungsmodells in einem Methodenaufruf integriert.
	 * <p>
	 * Aufgrund der IOS-Persistenz werden neben den IPS-Produktdatenprojekten
	 * zusätzlich IPS-Modellprojekte hinzugezogen.
	 */
	private AbstractRuntimeRepository createKfzRuntimeRepository() {
		AbstractRuntimeRepository hrVmProduktRepo = (AbstractRuntimeRepository) getModelRepository();
		hrVmProduktRepo.addDirectlyReferencedRepository(
				createRepository("at/faktorzehn/kfz/produkte/internal/faktorips-repository-toc.xml"));
		return hrVmProduktRepo;
	}

}
