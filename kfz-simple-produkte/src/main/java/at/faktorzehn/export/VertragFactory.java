package at.faktorzehn.export;

import at.faktorzehn.kfz.model.KfzProdukt;
import at.faktorzehn.kfz.model.KfzVertrag;

public class VertragFactory {
	private final KfzProdukt kfzProdukt;

	public VertragFactory(KfzProdukt kfzProdukt) {
		this.kfzProdukt = kfzProdukt;
	}

	public KfzVertrag createKfzVertrag() {
		KfzVertrag kfzVertag = this.kfzProdukt.createKfzVertrag();
		return kfzVertag;
	}
}
